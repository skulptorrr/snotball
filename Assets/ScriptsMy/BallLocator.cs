﻿using UnityEngine;

public class BallLocator : MonoBehaviour
{

    private Rigidbody _ball;

    private float _groundHeight ;

    public float FixHeiht=0.5f;

	// Use this for initialization
	void Start ()
	{
        _ball = GameObject.Find("Ball").GetComponent<Rigidbody>();

        _groundHeight = GameObject.Find("Ground").transform.position.y + FixHeiht;
	}
	
	// Update is called once per frame
	void Update ()
	{
        transform.position = new Vector3(_ball.transform.localPosition.x, _groundHeight, _ball.transform.localPosition.z);
	}
}
