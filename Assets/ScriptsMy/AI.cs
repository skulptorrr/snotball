﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;

public struct BallTrajectoryPoint {
	public Vector3 pos;
	public float time;
}

public class AI : MonoBehaviour
{
    [HideInInspector] public bool Enabled =  true;

    private SphereCollider _ballCollision;
    private Rigidbody _ballRigidbody;
    private PlayerController _p1C;

    private GameObject _ballObj;
    private LineRenderer _ballLineRenderer;

    private float _hGround ;

	List<BallTrajectoryPoint> path = new List<BallTrajectoryPoint>();
	List<BallTrajectoryPoint> pathFromMax = new List<BallTrajectoryPoint>();

    private PlayerController _playerControlls;
    private float _playerPosition;
    private bool _needToChangePosition;

    private float _shortMoveDistance = 0.5f;

    private float _leftBorderX;
    private float _rightBorderX;
    private float _walkSpeed;
    private float _jumpStartSpeed;
    private float _bodyHeight = 1.8f;
	private float _jumpMaxY;
	private float _jumpMaxTime;
	private BallTrajectoryPoint _pathMaxYPoint;
    
	void Start ()
	{
	    _playerControlls = GetComponent<PlayerController>();

        _leftBorderX = GameObject.Find("Net_Net").transform.position.x;
        _rightBorderX = GameObject.Find("palisade_section_Final").transform.position.x;
	    _walkSpeed = gameObject.GetComponent<PlayerController>().PlayerWalkSpeed;
        
	    _ballObj = GameObject.Find("Ball");
        _ballRigidbody = _ballObj.GetComponent<Rigidbody>();
        _ballCollision = _ballObj.GetComponent<SphereCollider>();
        _ballLineRenderer = _ballObj.GetComponent<LineRenderer>();
		_ballLineRenderer.SetVertexCount(500);

        _p1C = this.GetComponent<PlayerController>();

        _hGround = GameObject.Find("Ground").transform.position.y - 0.12f;

		_bodyHeight = GetComponent<MeshFilter>().mesh.bounds.size.y * transform.localScale.y;

		path.Capacity = 1000;
		pathFromMax.Capacity = 1000;

		// TODO: make real calculation
		_jumpStartSpeed = 11.6f;

		GetJumpHeight();

	}
	
	private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ShortMoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            ShortMoveRight();
        }


        UpdatePlayerPosition();
    }

    public void BallHited()
    {
        var ballVelocity = _ballRigidbody.velocity;

        float vY =  _ballRigidbody.velocity.y;
        float h0 = _ballRigidbody.position.y - _hGround - _ballCollision.radius - 0.1f;
        float g = Physics.gravity.y;

		CalculatePath(_ballRigidbody.position, _ballRigidbody.velocity, Physics.gravity, _ballRigidbody.drag, _hGround);

		_ballLineRenderer.enabled = false;
		/*
		_ballLineRenderer.SetVertexCount(path.Count);
		for (int i = 0; i < path.Count; i++)
        {
			_ballLineRenderer.SetPosition(i, path[i].pos);
        }*/

		MakeDecesion();
    }

	private void MakeDecesion(){
		float intersectionY = _hGround + _bodyHeight;
		intersectionY = _jumpMaxY;

		BallTrajectoryPoint targetPoint = pathFromMax[0];

		foreach (var point in pathFromMax){
			if (Mathf.Abs(point.pos.y - intersectionY) < Mathf.Abs(targetPoint.pos.y - intersectionY)){
				targetPoint = point;
			}
		}

		if (targetPoint.pos.x > _leftBorderX && targetPoint.pos.x < _rightBorderX)
		{
			var rndOffset = (float)Random.Range(10,20) / 100f;
			MoveToXPoint(targetPoint.pos.x + 0.2f);
			LeanTween.delayedCall(targetPoint.time - _jumpMaxTime, PerformJump);
		}
	}

    private void MoveToXPoint(float xPoint)
    {
        //if (MusicEnabled)
        //{
        //    _playerPosition = xPoint;
        //    _needToChangePosition = true;
        //}
    }

    private void ShortMoveLeft()
    {
        MoveToXPoint(_playerControlls.gameObject.transform.position.x - _shortMoveDistance);
    }

    private void ShortMoveRight()
    {
        MoveToXPoint(_playerControlls.gameObject.transform.position.x + _shortMoveDistance);
    }

    private void PerformJump()
    {
        //if (MusicEnabled)
        //{
        //    _p1C.Jump.UserAsksForAction = true;
        //}
        //else
        //{
        //    _p1C.Jump.UserAsksForAction = false;
        //}
    }
    
    private void UpdatePlayerPosition()
    {
        if (!_p1C.IsHumanPlayer && _needToChangePosition)
        {
            float deviation = 0.05f;
            bool isInCorrectRange = (transform.position.x > _playerPosition - deviation && transform.position.x < _playerPosition + deviation);

            if (isInCorrectRange)
            {
                _playerControlls.Left.UserAsksForAction = false;
                _playerControlls.Right.UserAsksForAction = false;
                _needToChangePosition = false;
            }
            else
            {
                if (transform.position.x < _playerPosition)
                {
                    _playerControlls.Right.UserAsksForAction = true;
                    _playerControlls.Left.UserAsksForAction = false;
                }
                else
                {
                    _playerControlls.Left.UserAsksForAction = true;
                    _playerControlls.Right.UserAsksForAction = false;
                }
            }
        }
    }

    private void Freez(int i)
    {
        Time.timeScale = 0;
        Debug.Log("Freez");
    }

    private void Unfreez()
    {
        Time.timeScale = 0.1f;
    }
	
	private List<BallTrajectoryPoint> CalculatePath(Vector3 startPos, Vector3 velocity, Vector3 g, float drag, float groundY, float dt = 1f / 30f)
	{
		BallTrajectoryPoint point;
		BallTrajectoryPoint prevPoint;

		float t = 0;

		path.Clear();
		pathFromMax.Clear();
		var v = velocity;

		point.pos = startPos;
		point.time = t;
		path.Add(point);

		prevPoint = point;

		_pathMaxYPoint = point;

		while(point.pos.y >= groundY){
			float dragForceMagnitude = v.magnitude*drag;
			Vector3 dragForce = -v.normalized*dragForceMagnitude;
			Vector3 f = g + dragForce; // FULL FORCE

			point.pos.x = prevPoint.pos.x + v.x * dt + (f.x * dt * dt) / 2f;
			point.pos.z = prevPoint.pos.z + v.z * dt + (f.z * dt * dt) / 2f;
			point.pos.y = prevPoint.pos.y + v.y * dt + (f.y * dt * dt) / 2f;

			v = v + f*dt;
			t += dt;

			point.time = t;
			path.Add(point);

			if (point.pos.y >= _pathMaxYPoint.pos.y)
				_pathMaxYPoint = point;
			else
				pathFromMax.Add(point);

			prevPoint = point;
		}

		return path;
		
	}

	private List<BallTrajectoryPoint> CalculateJumpPath()
	{
		Vector3 startPos = new Vector3(0, transform.position.y + _bodyHeight, 0);
		Vector3 velocity = new Vector3(0, _jumpStartSpeed, 0);

		CalculatePath(startPos, velocity, Physics.gravity, _p1C.Rbody.drag, _hGround);

		return path;
	}

	private void GetJumpHeight(){
		CalculateJumpPath();

		_ballLineRenderer.SetVertexCount(path.Count);
		for (int i = 0; i < path.Count; i++)
		{
			_ballLineRenderer.SetPosition(i, path[i].pos);
		}

		BallTrajectoryPoint prevPoint = path[path.Count / 7];
		
//		foreach (var point in path){
//			if (point.pos.y < prevPoint.pos.y)
//			{
				_jumpMaxY =	prevPoint.pos.y;
				_jumpMaxTime = prevPoint.time;
//				return;
//			}
//		}
	}
}
