﻿using System;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;


public class GameplayBallBehavior : MonoBehaviour
{
    public int BallHitsAllowed;
    
    private Rigidbody _rbody;

    private int _playerHits;
    
    private string _playerLastTimeTouchedBall = "0";
    
    public float BallHeightPositionOnGameStart = 2.5f;

    private float _fieldXCenterPosition;

    private float FieldLength
    {
        get { return _gameConfig.FieldLength; }
    }

    public int Player1Score
    {
        get { return _menuLib.InGameMenuObj.Player1Score; }
        set { _menuLib.InGameMenuObj.Player1Score = value; }
    }

    public int Player2Score
    {
        get { return _menuLib.InGameMenuObj.Player2Score; }
        set { _menuLib.InGameMenuObj.Player2Score = value; }
    }

    public int ScoreToWin
    {
        get { return _menuLib.InGameMenuObj.ScoreToWin; }
        set { _menuLib.InGameMenuObj.ScoreToWin = value; }
    }

    private bool PlayersTouchedBall
    {
        get { return _rbody.useGravity; }
        set { _rbody.useGravity = value; }
    }

    public bool DoubleGoalModeEnabled = true;

    private float _heightOfSpaceUnderNet = 7.5f;

    [HideInInspector]
    public int LastGoalWasByPlayer;
    
    private GameSettings _gameConfig;
    
    private PlayerController _player1Controller;
    private PlayerController _player2Controller;
    private Rigidbody _player1Rigidbody;
    private Rigidbody _player2Rigidbody;

    private AI _player2Ai;
    
    private AudioLib _audioLib;

    private MusicObjScript _musicObj;

    private ScreenMessage _screenMessage;

    private MenuManager _menuLib;

    private Stopwatch _gameStopwatch = new Stopwatch();
    private int _player1CoinsScore;
    private int _coinsFuckUp = -1000;
    private int _coinsIn1Goal = 5000;

    private void AddStopwatchCoins()
    {
        var rnd = new System.Random();
        int randomCoins = rnd.Next(4, 321);

        if (_gameStopwatch.ElapsedMilliseconds < 10000)
        {
            randomCoins += 3000;
        }
        else if (_gameStopwatch.ElapsedMilliseconds < 30000)
        {
            randomCoins += 1500;
        }
        else if (_gameStopwatch.ElapsedMilliseconds < 60000)
        {
            randomCoins += 500;
        }
        else if (_gameStopwatch.ElapsedMilliseconds < 90000)
        {
            randomCoins += 200;
        }

        _player1CoinsScore += randomCoins;
        //Debug.Log("_gameStopwatch Elapsed = " + _gameStopwatch.Elapsed + " Coins scored:" + randomCoins);
    }

    void Start () {
        _rbody = GetComponent<Rigidbody>();

        GameObject player1 = GameObject.Find("Player1");
        GameObject player2 = GameObject.Find("Player2");

        _player1Controller = player1.GetComponent<PlayerController>();
        _player2Controller = player2.GetComponent<PlayerController>();

        _player1Rigidbody = player1.GetComponent<Rigidbody>();
        _player2Rigidbody = player2.GetComponent<Rigidbody>();


        _player2Ai = _player2Controller.gameObject.GetComponent<AI>();

	    _gameConfig = GameObject.Find("GameSettings").GetComponent<GameSettings>();

	    _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

	    _audioLib = GameObject.Find("AudioLib").GetComponent<AudioLib>();

	    _musicObj = GameObject.Find("MusicObj").GetComponent<MusicObjScript>();

        _fieldXCenterPosition = GameObject.Find("Net_Net").transform.position.x;

        _screenMessage = new ScreenMessage(_menuLib.InGameMenuObj.InGameMessage);

	    StartNewMatch();
	}
	
    private void FixedUpdate()
    {
        if (PlayersTouchedBall)
        {
            IsBallFliedUnderTheNet();
            CheckForPlayersWalksOnEnemySide();
        }
    }

    void OnCollisionEnter(Collision theCollision)
    {
        if (!PlayersTouchedBall)
        {
            if (theCollision.gameObject.name.Contains("Player"))
            {
                PlayersTouchedBall = true;
                _gameStopwatch.Start();
            }
        }

        CheckBallHitGround(theCollision);

        CheckForPlayerBallHits(theCollision);
    }

    private void OnCollisionExit(Collision theCollision)
    {
        if (_rbody.velocity.y > 20)
        {
            Debug.Log(_rbody.velocity.y + " was changed");
            _rbody.velocity = new Vector3(_rbody.velocity.x, 20, _rbody.velocity.z);
        }
        
        GameObject.Find("Player2").GetComponent<AI>().BallHited();
    }

    private void CheckBallHitGround(Collision theCollision)
    {
        if (theCollision.gameObject.name == "Ground")
        {
            if (_playerLastTimeTouchedBall == "Player1")
            {
                if (transform.position.x > _fieldXCenterPosition)
                {
                    ShowMessageLeftSideGoalAndStartNewRound();
                }
                else
                {
                    ShowMessageLeftSideFuckupAndStartNewRound();
                }
            }
            else
            {
                if (transform.position.x > _fieldXCenterPosition)
                {
                    ShowMessageRightSideFuckupAndStartNewRound();
                    
                }
                else
                {
                    ShowMessageRightSideGoalAndStartNewRound();
                }
            }

        }
    }

    private void CheckForPlayerBallHits(Collision theCollision)
    {
        if (theCollision.gameObject.name.Contains("Player"))
        {
            _playerHits += 1;

            if (theCollision.gameObject.name == _playerLastTimeTouchedBall)
            {
                Debug.Log(_playerLastTimeTouchedBall + " ball hits: " + _playerHits);

                if (_playerHits == BallHitsAllowed + 1)
                {
                    Debug.Log(_playerLastTimeTouchedBall + " Fucked up!");

                    PlaySound(_audioLib.WhistleFuckUp);

                    if (_playerLastTimeTouchedBall == "Player2")
                    {
                        ShowMessageRightSideFuckupAndStartNewRound();
                    }
                    else
                    {
                        ShowMessageLeftSideFuckupAndStartNewRound();
                    }
                }
            }
            else
            {
                _playerLastTimeTouchedBall = theCollision.gameObject.name;
                _playerHits = 0;
            }
        }   
    }
    
    private void GoalScoredBy(string playerName)
    {
        if (playerName == "Player1")
        {
            Plus1GoalForPlayer(1);
        }
        else
        {
            Plus1GoalForPlayer(2);
        }

        if (Player1Score == ScoreToWin || Player2Score == ScoreToWin)
        {
            Time.timeScale = 1;
            ShowResultScreen();
        }
    }

    private void Plus1GoalForPlayer(int playerNumber, string messageToLog)
    {
        Debug.Log(messageToLog);
        Plus1GoalForPlayer(playerNumber);
    }

    private void Plus1GoalForPlayer(int playerNumber)
    {
        string exceptionError = "Mistakes in code! Cannot find such player number!";
        int goalScoredSide=1;

        if (Player1Score < ScoreToWin && Player2Score < ScoreToWin)
        {
            if (DoubleGoalModeEnabled)
            {
                if (LastGoalWasByPlayer == 1)
                {
                    switch (playerNumber)
                    {
                        case 1:
                            Player1Score += 1;
                            goalScoredSide = 1;
                            break;
                        case 2:
                            break;
                        default:
                            throw new Exception(exceptionError);
                    }
                }
                else if (LastGoalWasByPlayer == 2) //needed for first goal
                {
                    switch (playerNumber)
                    {
                        case 1:
                            break;
                        case 2:
                            Player2Score += 1;
                            goalScoredSide = 2;
                            break;
                        default:
                            throw new Exception(exceptionError);
                    }
                }

                LastGoalWasByPlayer = playerNumber;
            }
            else
            {
                switch (playerNumber)
                {
                    case 1:
                        Player1Score += 1;
                        goalScoredSide = 1;
                        break;
                    case 2:
                        Player2Score += 1;
                        goalScoredSide = 2;
                        break;
                    default:
                        throw new Exception(exceptionError);
                }   
            }
        }

        if (Player1Score < ScoreToWin && Player2Score < ScoreToWin)
        {
            LeanTween.delayedCall(_messageShowTime, () => { StartNewRoundBallIsOnSide(goalScoredSide); }).setUseEstimatedTime(true);
            Time.timeScale = _slowdownedTimeScale;
            Debug.Log("Requested StartNewRound");
        }
    }
    
    private void StartNewRoundBallIsOnSide(int side)
    {
        //side = 1;

        _rbody.velocity = Vector3.zero;
        _rbody.angularVelocity = Vector3.zero;

        var player1Position = _player1Controller.gameObject.transform.position;
        var player2Position = _player2Controller.gameObject.transform.position;

        float ground = 5.42f;

        float player1X = _fieldXCenterPosition - FieldLength / 4;
        float player2X = _fieldXCenterPosition + FieldLength / 4;

        switch (side)
        {
            case 1:
                transform.position = new Vector3(player1X + 0.5f, ground + BallHeightPositionOnGameStart, player1Position.z);
                break;
            case 2:
                transform.position = new Vector3(player2X - 0.5f, ground + BallHeightPositionOnGameStart, player1Position.z);
                break;
            default:
                throw new Exception("mistakes in code!");
        }

        Vector3 player1XeroLocation = new Vector3(player1X, ground, player1Position.z);
        Vector3 player2XeroLocation = new Vector3(player2X, ground, player2Position.z);

        _player1Controller.gameObject.transform.position = player1XeroLocation;
        _player2Controller.gameObject.transform.position = player2XeroLocation;

        _player1Controller.ResetScale();
        _player2Controller.ResetScale();

        _player1Rigidbody.velocity = Vector3.zero;
        _player2Rigidbody.velocity = Vector3.zero;

        PlaySound(_audioLib.WhistleGameStart);

        _playerHits = 0;

        PlayersTouchedBall = false;

        Debug.Log("New Round started!");

        _goalWasScored = false;

        Time.timeScale = 1f;
        _screenMessage.Reset();

        Debug.Log("SCORE: " + Player1Score + ":" + Player2Score);
        Debug.Log("Player1 Coins: " + _player1CoinsScore);

        _gameStopwatch.Reset();
    }
    
    public void StartNewMatch()
    {
        Player1Score = 0;
        Player2Score = 0;

        LastGoalWasByPlayer = 0;

        _playerLastTimeTouchedBall = "0";
        
        var rnd = new System.Random();
        int randomSide = rnd.Next(1, 21);

        StartNewRoundBallIsOnSide( Convert.ToInt32( randomSide%2 == 0) + 1 );
    }

    private void IsBallFliedUnderTheNet()
    {
        bool closeToNetXposition = (_fieldXCenterPosition + 0.3f > transform.position.x && _fieldXCenterPosition - 0.3f < transform.position.x);
        
        if (transform.position.y <= _heightOfSpaceUnderNet && closeToNetXposition)
        {
            if (_playerLastTimeTouchedBall == "Player1")
            {
                ShowMessageLeftSideFuckupAndStartNewRound();
            }
            else
            {
                ShowMessageRightSideFuckupAndStartNewRound();
            }
        }
    }

    private void CheckForPlayersWalksOnEnemySide()
    {
        var limitXPositionForPlayer1 = _fieldXCenterPosition + 0.5;
        var limitXPositionForPlayer2 = _fieldXCenterPosition - 0.5;

        if (_player1Controller.gameObject.transform.position.x > limitXPositionForPlayer1)
        {
            ShowMessageLeftPlayerWalkedOnRightSide();
        }

        if (_player2Controller.gameObject.transform.position.x < limitXPositionForPlayer2)
        {
            ShowMessageRightPlayerWalkedOnLeftSide();
        }
    }

    private void PlaySound(AudioClip audioClip)
    {
        if (_musicObj.SoundsEnabled)
        {
            GetComponent<AudioSource>().PlayOneShot(audioClip);
        }
    }
    
    private bool _goalWasScored;

    private float _messageShowTime = 2.5f;
    private float _slowdownedTimeScale = 0.1f;
    private int _plus1FontSize = 350;

    private void ShowMessageLeftSideGoalAndStartNewRound()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Left side scored goal!");

            _screenMessage.ShowMessage(_messageShowTime, "+1", TextAnchor.MiddleLeft, _plus1FontSize);

            GoalScoredBy("Player1");
            PlaySound(_audioLib.WhistleSuccess);

            AddStopwatchCoins();
        }
    }
    
    private void ShowMessageRightSideGoalAndStartNewRound()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Right side scored goal!");

            _screenMessage.ShowMessage(_messageShowTime, "+1", TextAnchor.MiddleRight, _plus1FontSize);

            GoalScoredBy("Player2");
            PlaySound(_audioLib.WhistleSuccess);
        }
    }

    private void ShowMessageLeftSideFuckupAndStartNewRound()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Left side fucked up. Right side scored goal!");

            _screenMessage.ShowMessage(_messageShowTime, "+1", TextAnchor.MiddleRight, _plus1FontSize);

            GoalScoredBy("Player2");

            PlaySound(_audioLib.WhistleFuckUp);

            _player1CoinsScore += _coinsFuckUp;
        }
    }

    private void ShowMessageRightSideFuckupAndStartNewRound()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Right side fucked up. Left side scored goal!");

            _screenMessage.ShowMessage(_messageShowTime, "+1", TextAnchor.MiddleLeft, _plus1FontSize);

            GoalScoredBy("Player1");

            PlaySound(_audioLib.WhistleFuckUp);
            
            AddStopwatchCoins();
        }
    }

    private void ShowMessageLeftPlayerWalkedOnRightSide()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Do not walk on enemy side due to game process!");

            _screenMessage.ShowMessage(_messageShowTime, "DO NOT WALK ON\r\n    ENEMY SIDE!    ", TextAnchor.MiddleLeft);

            GoalScoredBy("Player2");

            PlaySound(_audioLib.WhistleFuckUp);
            
            _player1CoinsScore += _coinsFuckUp;
        }
    }

    private void ShowMessageRightPlayerWalkedOnLeftSide()
    {
        if (_goalWasScored == false)
        {
            _goalWasScored = true;
            Debug.Log("Do not walk on enemy side due to game process!");

            GoalScoredBy("Player1");

            _screenMessage.ShowMessage(_messageShowTime, "DO NOT WALK ON\r\n   ENEMY SIDE!   ", TextAnchor.MiddleRight);

            PlaySound(_audioLib.WhistleFuckUp);

            AddStopwatchCoins();
        }
    }

    private void ShowResultScreen()
    {
        _player1Controller.IsHumanPlayer = false;
        _player1Controller.JumpForever = true;
        _player2Ai.Enabled = false;

        if (Player1Score > Player2Score)
        {
            _player1CoinsScore = _coinsIn1Goal*(Player1Score - Player2Score);
        }
        _menuLib.InGameMenuObj.Enabled = false;
        _menuLib.ResultScreenMenu.Enabled = true;
        _menuLib.ResultScreenMenu.UserScore += _player1CoinsScore;
    }
}

public class ScreenMessage
{
    private GameObject _msgObj;
    private Text _msgObjTextComponent;

    private float _originalYPosition;

    private int _originalFontSize;

    public ScreenMessage(GameObject textObj)
    {
        _msgObj = textObj;
        _msgObjTextComponent = _msgObj.GetComponent<Text>();
        _originalYPosition = _msgObj.transform.position.y;
        _originalFontSize = _msgObjTextComponent.fontSize;
    }

    public void Reset()
    {
        _msgObjTextComponent.enabled = false;
        _msgObj.transform.position = new Vector3(_msgObj.transform.position.x, _originalYPosition, _msgObj.transform.position.z);
    }

    public void ShowMessage(float messageShowTime, string message, TextAnchor textAnchor, int fontSize)
    {
        Reset();

        _msgObjTextComponent.fontSize = fontSize;

        _msgObjTextComponent.enabled = true;
        _msgObjTextComponent.text = message;
        _msgObjTextComponent.alignment = textAnchor;

        LeanTween.moveY(_msgObj, _originalYPosition + 0.6f, messageShowTime).setUseEstimatedTime(true);
    }

    public void ShowMessage(float messageShowTime, string message, TextAnchor textAnchor)
    {
        ShowMessage(messageShowTime, message, textAnchor, _originalFontSize);
    }
}