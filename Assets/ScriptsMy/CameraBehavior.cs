﻿using UnityEngine;
using System.Collections;

public class CameraBehavior : MonoBehaviour {

    [Header("Vertical camera rotation")]
    public float FieldXBottomPosition1 = 17f;
    public float FieldXHeight = 10f;
    [Range(0.0f, 20.0f)]
    public float VerticalAngleRange=5;

    private float _fieldHeightInPercent;
    
    [Header("Horizontal camera rotation")]
    [Range(0.0f, 20.0f)]
    public float HorizontalAngleRange = 10;

    public bool InverseHorisontalAngle;
    
    [Header("Horizontal camera position")]
    [Range(0.0f, 10.0f)]
    public float HorizontalPositionRange = 4f;

    [Header("Depth camera position")]
    public float FieldXBottomPositionForCameraDepth = 13f;
    [Range(0.0f, 10.0f)]
    public float DepthPositionRange = 3f;
    private float _depthMinimumPosition;

    private float _fieldLength;

    private GameObject _ball;
    private Vector3 BallPosition
    {
        get { return _ball.transform.position; }
    }

    private float _fieldXCenterPosition;
    private float _fieldXLeftPosition;
    private float _fieldXRightPosition;
    private float _fieldLengthInPercent;

    private float _percentsRangeFromCenterHorizontal;
    private float _percentsRangeFromBottomVertical;

    //ATTENTION! This is horizontal rotator!
    private float _cameraYCenterAngle = 0;
    private float _cameraYAngleStepInPercent;

    // ATTENTION! THIS IS vertical rotator!
    private float _cameraXBottomAngle = 0;
    private float _cameraXAngleStepInPercent;

    private float _cameraXCenterPosition;
    private float _cameraPozitionStepInPercent;
    
    

    
    
    private bool BallInHorizontalArea
    {
        get { return InRange(BallPosition.x, _fieldXLeftPosition, _fieldXRightPosition); }
    }

    void Start () 
    {
        _ball = GameObject.Find("Ball");

        _fieldLength = GameObject.Find("GameSettings").GetComponent<GameSettings>().FieldLength;

        _fieldXCenterPosition = GameObject.Find("Net_Net").transform.position.x;
        _fieldXLeftPosition = _fieldXCenterPosition - _fieldLength / 2;
        _fieldXRightPosition = _fieldXCenterPosition + _fieldLength / 2;
        _fieldLengthInPercent = _fieldLength / 100;

        _depthMinimumPosition = transform.position.z;

        _fieldHeightInPercent = FieldXBottomPosition1 - FieldXHeight/100;

        _cameraYAngleStepInPercent = 1.0f / 180 * HorizontalAngleRange / 100; // range must be 0..1; 1 = 100 % = 180 ^0 - have no idea why so

        _cameraXAngleStepInPercent = VerticalAngleRange / 100;

        _cameraPozitionStepInPercent = HorizontalPositionRange / 100;

        _cameraXCenterPosition = GameObject.Find("Net_Net").transform.position.x;
    }
	
	void Update ()
	{
        if (BallPosition.x > FieldXBottomPosition1)
        {
            _percentsRangeFromBottomVertical = FieldXHeight - FieldXBottomPosition1 / _fieldHeightInPercent;
        }

        _percentsRangeFromCenterHorizontal = Mathf.Abs((BallPosition.x - _fieldXCenterPosition) / _fieldLengthInPercent);
	    
	    UpdateHorizontalAngle();
        
        UpdateHorizontalPosition();

        UpdateVerticalAngle();
        
        UpdateCameraDepthPosition();
	}

    private void UpdateHorizontalAngle()
    {
        float y = transform.rotation.y;
        
        if (BallPosition.x < _fieldXCenterPosition)
        {
            if (InverseHorisontalAngle)
            {
                y = _cameraYCenterAngle + _percentsRangeFromCenterHorizontal * _cameraYAngleStepInPercent;
            }
            else
            {
                y = _cameraYCenterAngle - _percentsRangeFromCenterHorizontal * _cameraYAngleStepInPercent;    
            }
            
        }
        else if (BallPosition.x > _fieldXCenterPosition)
        {
            if (InverseHorisontalAngle)
            {
                y = _cameraYCenterAngle - _percentsRangeFromCenterHorizontal * _cameraYAngleStepInPercent;
            }
            else
            {
                y = _cameraYCenterAngle + _percentsRangeFromCenterHorizontal * _cameraYAngleStepInPercent;
            }
        }

        transform.rotation = new Quaternion(transform.rotation.x, y, transform.rotation.z, transform.rotation.w);
    }

    private void UpdateHorizontalPosition()
    {
        float x = transform.position.x;

        if (BallPosition.x < _fieldXCenterPosition)
        {
            x = _cameraXCenterPosition - _percentsRangeFromCenterHorizontal * _cameraPozitionStepInPercent;
        }
        else if (BallPosition.x > _fieldXCenterPosition)
        {
            x = _cameraXCenterPosition + _percentsRangeFromCenterHorizontal * _cameraPozitionStepInPercent;
        }

        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    private void UpdateVerticalAngle()
    {
        if (BallPosition.y > FieldXBottomPosition1 && BallPosition.y < FieldXBottomPosition1 + FieldXHeight)
        {
            _percentsRangeFromBottomVertical = Mathf.Abs((BallPosition.y - FieldXBottomPosition1)/FieldXHeight);

            float x = _cameraXBottomAngle - _percentsRangeFromBottomVertical*_cameraXAngleStepInPercent;

            transform.rotation = new Quaternion(x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        }
    }

    private void UpdateCameraDepthPosition()
    {
        if (BallPosition.y > FieldXBottomPositionForCameraDepth && BallPosition.y < FieldXBottomPositionForCameraDepth + FieldXHeight)
        {
            _percentsRangeFromBottomVertical = Mathf.Abs((BallPosition.y - FieldXBottomPositionForCameraDepth) / FieldXHeight);

            float z = _depthMinimumPosition - _percentsRangeFromBottomVertical;

            transform.position = new Vector3(transform.position.x, transform.position.y, z);
        }
    }

    private static bool InRange(float value, float from, float to)
    {
        return (value > from && value < to);
    }
}
