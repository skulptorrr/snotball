﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour {
    [Header("GUI settings")]
    [Range(0.1f, 0.5f)]
    public float TreeAnimationWindPower;
    
    [HideInInspector]
    public float FieldLength = 20f;
    
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}
