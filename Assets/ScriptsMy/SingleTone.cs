﻿using UnityEngine;
using System.Collections;

public class SingleTone : MonoBehaviour {

    static SingleTone Instance;
    
    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
