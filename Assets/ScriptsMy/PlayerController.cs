﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public Rigidbody Rbody;
    private bool _isGrounded = false;
    private bool _touchingWallLeft = false;
    private bool _touchingWallRight = false;

    //[HideInInspector]
    public float PlayerJumpPower = 12f;

    [HideInInspector]
    public float PlayerWalkSpeed = 5f;

    private AudioSource _audioSource;

    private AudioLib _audioLib;

    private SoundObjScript _gameSound;

    [HideInInspector]
    public Vector3 Accelerometer = Vector3.zero;

    public bool IsHumanPlayer;

    private MenuManager _menuLib;

    private bool _walkLeftNeeded {
        get { return Left.UserAsksForAction; }
    }
    private bool _walkRightNeeded
    {
        get { return Right.UserAsksForAction; }
    }

    public bool JumpForever
    {
        get { return Jump.UserAsksForAction; }
        set { Jump.UserAsksForAction = value; }
    }

    public PlayerControllsPanel Left;
    public PlayerControllsPanel Right;
    public PlayerControllsPanel Jump;

    //public delegate void BallCollisionHeandler();//Create Event Hendler

    //public static event BallCollisionHeandler OnBallCollisionExited; // Create of event

    private PlayerAnimator _playerAnimator;

    private void Start()
    {
        _gameSound = GameObject.Find("SoundObj").GetComponent<SoundObjScript>();

        _audioLib = GameObject.Find("AudioLib").GetComponent<AudioLib>();

        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

        var playerMesh = GameObject.Find(gameObject.name + "Mesh");
        
        _audioSource = GetComponent<AudioSource>();

        Rbody = GetComponent<Rigidbody>();

        Left.IsHuman = IsHumanPlayer;
        Right.IsHuman = IsHumanPlayer;
        Jump.IsHuman = IsHumanPlayer;

        _playerAnimator = new PlayerAnimator(playerMesh);
    }

    private void FixedUpdate()
    {
        if (transform.position.y > 11f)
        {
            Debug.Log("BUG!!! SuperJump bug reproduced by" + this.name + "!!!!");
        }

        Accelerometer = Input.acceleration;

        if (!_menuLib.PauseMenuObj.Enabled)
        {
            DoJumpIfNeeded();

            DoWalkActionIfNeeded();
        }
    }

    private void OnCollisionEnter(Collision theCollision)
    {
        if (theCollision.gameObject.name == "Ground")
        {
            _isGrounded = true;
            _touchingWallLeft = false;
            _touchingWallRight = false;

            _playerAnimator.HitGroundAnimation();

            if (_gameSound.Enabled)
            {
                _audioSource.PlayOneShot(_audioLib.JumpEnd);
            }
        }
        else if (!_isGrounded && theCollision.gameObject.name != "Ball")
        {
            if (theCollision.relativeVelocity.x > 0)
            {
                _touchingWallLeft = true;
            }
            else
            {
                _touchingWallRight = true;
            }
        }

    }
    
    private void DisableControllsBlock()
    {
        _touchingWallLeft = false;
        _touchingWallRight = false;
    }

    public void ResetScale()
    {
        if (_playerAnimator != null)
        {
            _playerAnimator.ResetScale();
        }
    }

    static void Logger(string s)
    {
        Console.WriteLine(s);
    }

    public void DoJumpIfNeeded()
    {
        if (Jump.UserAsksForAction)
        {
            DoJump();
            Jump.UserAsksForAction = false;
        }
    }

    private void DoWalkActionIfNeeded()
    {
        if (_walkLeftNeeded && !_touchingWallLeft)
        {
            Rbody.velocity = new Vector3(-PlayerWalkSpeed, Rbody.velocity.y, Rbody.velocity.z);
            _playerAnimator.StartWalking(_isGrounded);
        }
        else if (_walkRightNeeded && !_touchingWallRight)
        {
            Rbody.velocity = new Vector3(PlayerWalkSpeed, Rbody.velocity.y, Rbody.velocity.z);
            _playerAnimator.StartWalking(_isGrounded);
        }
        else if (Time.timeScale == 1)
        {
            //Stop Walking
            Rbody.velocity = new Vector3(0, Rbody.velocity.y, Rbody.velocity.z);
            _playerAnimator.StopWalking(_isGrounded);
        }
    }

    public void DoJump()
    {
        if (_isGrounded)
        {
            Rbody.velocity = new Vector3(Rbody.velocity.x, PlayerJumpPower, Rbody.velocity.z);

            _playerAnimator.JumpAnimation();

            _isGrounded = false;

            if (_gameSound.Enabled)
            {
                _audioSource.PlayOneShot(_audioLib.JumpStart);
            }
        }
    }
}

public class PlayerAnimator : MonoBehaviour
{
    private GameObject _playerMesh;

    private Vector3 _playerScaleOriginal;
    private Vector3 _playerScaleInJump;
    private Vector3 _playerScaleAfterJump;
    private Vector3 _playerScaleWalking;

    private bool _isWalking;
    private bool _isGrounded;

    private float _walkingAnimationTime = 0.17f;
    public PlayerAnimator(GameObject playerMesh)
    {
        _playerMesh = playerMesh;

        _playerScaleInJump = CalculateJumpAnimationScale(1.25f);
        _playerScaleAfterJump = CalculateJumpAnimationScale(0.85f);
        _playerScaleWalking = CalculateJumpAnimationScale(0.88f);
    }
    
    private Vector3 CalculateJumpAnimationScale(float verticalScale)
    {
        _playerScaleOriginal = _playerMesh.transform.localScale;

        var newY = _playerMesh.transform.localScale.y * verticalScale;
        var newX = _playerMesh.transform.localScale.x / verticalScale;

        return new Vector3(newX, newY, newX);
    }

    private LTDescr[] _animationsJump = new LTDescr[3];
    private LTDescr[] _animationsHitGround = new LTDescr[3];
    private LTDescr[] _animationsWalk = new LTDescr[4];

    private int _walkStatus = 0;

    public void JumpAnimation()
    {
        StopAnimation(_animationsHitGround);
        StopAnimation(_animationsWalk);

        //BeginJumpAnimation
        _animationsJump[0] = LeanTween.scale(_playerMesh, _playerScaleInJump, 0.1f).setEase(LeanTweenType.easeOutBounce);
        _animationsJump[0].onComplete = () => { _animationsJump[0] = null; };

        _animationsJump[1] = LeanTween.delayedCall(_playerMesh, 0.1f, () =>
        {
            //EndJumpAnimation
            _animationsJump[2] = LeanTween.scale(_playerMesh, _playerScaleOriginal, 0.5f).setEase(LeanTweenType.easeOutBounce);
            _animationsJump[2].onComplete = () => { _animationsJump[2] = null; };

            _animationsJump[1] = null;
        });
        
    }

    public void HitGroundAnimation()
    {
        if (!_isWalking)
        {
            StopAnimation(_animationsHitGround);
            StopAnimation(_animationsWalk);

            _animationsHitGround[0] = LeanTween.scale(_playerMesh, _playerScaleAfterJump, 0.1f).setEase(LeanTweenType.easeOutQuint);
            _animationsHitGround[0].onComplete = () => { _animationsHitGround[0] = null; };
            
            _animationsHitGround[1] = LeanTween.delayedCall(_playerMesh, 0.1f, () =>
            {
                _animationsHitGround[2] = LeanTween.scale(_playerMesh, _playerScaleOriginal, 0.3f).setEase(LeanTweenType.easeOutBounce);
                _animationsHitGround[2].onComplete = () => { _animationsHitGround[2] = null; };

                _animationsHitGround[1] = null;
            });
            
        }
    }

    #region Walking

        public void StartWalking(bool isGrounded)
        {
            _isGrounded = isGrounded;

            if (_isGrounded)
            {
                _isWalking = true;
            }

            WalkIfNeeded();
        }

        public void StopWalking(bool isGrounded)
        {
            _isWalking = false;
            _isGrounded = isGrounded;

            WalkIfNeeded();
        }

        void WalkIfNeeded()
        {
            //if (_isWalking && _isGrounded)
            //{
            //    StopAnimation(_animationsHitGround);

            //    if (_walkStatus == 0)
            //    {
            //        _animationsWalk[0] = LeanTween.scale(_playerMesh, _playerScaleWalking, _walkingAnimationTime);
            //        _animationsWalk[0].onComplete = () => { _animationsWalk[0] = null; };

            //        _animationsWalk[3] = LeanTween.delayedCall(_walkingAnimationTime, () =>
            //        {
            //            _walkStatus = 1;
            //            _animationsWalk[3] = null;
            //        });
            //    }
            //    else if (_walkStatus == 1)
            //    {
            //        _animationsWalk[1] = LeanTween.scale(_playerMesh, _playerScaleOriginal, _walkingAnimationTime);
            //        _animationsWalk[1].onComplete = () => { _animationsWalk[1] = null; };

            //        _animationsWalk[3] = LeanTween.delayedCall(_walkingAnimationTime, () =>
            //        {
            //            _walkStatus = 0;
            //            _animationsWalk[3] = null;
            //        });
            //    }
            //}
            //else if (!_isWalking && _isGrounded && _playerMesh.transform.localScale.y < _playerScaleOriginal.y)
            //{
            //    Time.timeScale = 0.3f;

            //    //StopAnimation(_animationsWalk);

            //    //_animationsWalk[2] = LeanTween.scale(_playerMesh, _playerScaleOriginal, _walkingAnimationTime/3);
            //    //_animationsWalk[2].onComplete = () => { _animationsWalk[2] = null; };

            //    //_animationsWalk[3] = LeanTween.delayedCall(_walkingAnimationTime, () =>
            //    //{
            //    //    _walkStatus = 0;
            //    //    _animationsWalk[3] = null;
            //    //});
            //}
        }


    #endregion Walking

    private void StopAnimation(LTDescr[] animations)
    {
        foreach (var anim in animations)
        {
            if (anim != null)
                anim.cancel();
        }
    }

    public void StopAllAnimations()
    {
        StopAnimation(_animationsJump);
        StopAnimation(_animationsHitGround);
        StopAnimation(_animationsWalk);
    }

    public void ResetScale()
    {
        _playerMesh.transform.localScale = _playerScaleOriginal;
    }
}