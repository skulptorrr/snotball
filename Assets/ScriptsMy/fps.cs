﻿using System;
using UnityEngine;

public class fps : MonoBehaviour {
	
	float _deltaTime = 0.0f;

    public Color TextColor = new Color(255f,0,0);

	const float FpsMeasurePeriod = 0.5f;
	private int _fpsAccumulator = 0;
	private float _fpsNextPeriod = 0;
	private float _currentFps;

    private string _fpsInfo = String.Empty;

    private GameSettings _gameSettings;

    public bool ExtremeModeEnabled;

    private int _pixelsInPercent;
    private int _screenH;
    private int _screenW;

    private float _indentionPx;
    private float _subScreenLength;
    private float _subScreenHeight;
    
    [Range(0.0f, 10.0f)]
    public float Indentation = 5f;

    public bool ShowFps = true;


	void Awake() {
          if (ExtremeModeEnabled)
          {
               //QualitySettings.vSyncCount = 0;  // 0 for no sync, 1 for panel refresh rate, 2 for 1/2 panel rate
          }

		Application.targetFrameRate = 90;
    }


    void Start()
    {
        _screenW = Screen.width;
        _screenH = Screen.height;
        _pixelsInPercent = (int)((float)_screenH / 100f);
        _indentionPx = Indentation * _pixelsInPercent;
        _subScreenLength = _screenW - _indentionPx * 2;
        _subScreenHeight = _screenH - _indentionPx * 2;
        
		_fpsNextPeriod = Time.realtimeSinceStartup + FpsMeasurePeriod;
	}
	
	void Update()
	{
        if (ShowFps)
	    {
            // measure average frames per second
            _fpsAccumulator++;
            if (Time.realtimeSinceStartup > _fpsNextPeriod)
            {
                _currentFps = (_fpsAccumulator / FpsMeasurePeriod);
                _fpsAccumulator = 0;
                _fpsNextPeriod += FpsMeasurePeriod;
            }

            _deltaTime = 1.0f / _currentFps;

            //_deltaTime += (Time._deltaTime - _deltaTime) * 0.1f;
	    }
	}


    private void OnGUI()
    {
        if (ShowFps)
        {
            float fps = 1.0f/_deltaTime;
            float msec = _deltaTime*1000.0f;
            _fpsInfo = string.Format("{0} fps ({1:0.0}ms)", fps, msec);

            DrawText(_fpsInfo, TextColor, 8, TextAnchor.LowerRight);
        }
    }
    

    private void DrawText(string text, Color color, float textHeightInPercents, TextAnchor tAnchor)
    {
        DrawText(text, color, textHeightInPercents, tAnchor, 0f, 0f);
    }

    private void DrawText(string text, Color color, float textHeightInPercents, TextAnchor tAnchor, float indentionVertical, float indentionHorisontal)
    {
        Rect rect;
        indentionHorisontal *= _pixelsInPercent;
        indentionVertical *= _pixelsInPercent;

        switch (tAnchor)
        {
            case TextAnchor.UpperLeft:
            case TextAnchor.UpperCenter:
            case TextAnchor.MiddleLeft:
            case TextAnchor.MiddleCenter:
                rect = new Rect(_indentionPx + indentionHorisontal, _indentionPx + indentionVertical, _subScreenLength, _subScreenHeight);
                break;

            case TextAnchor.UpperRight:
            case TextAnchor.MiddleRight:
                rect = new Rect(_indentionPx, _indentionPx + indentionVertical, _subScreenLength - indentionHorisontal, _subScreenHeight);
                break;

            case TextAnchor.LowerLeft:
            case TextAnchor.LowerCenter:
                rect = new Rect(_indentionPx + indentionHorisontal, _indentionPx, _subScreenLength, _subScreenHeight - indentionVertical);
                break;

            case TextAnchor.LowerRight:
                rect = new Rect(_indentionPx, _indentionPx, _subScreenLength - indentionHorisontal, _subScreenHeight - indentionVertical);
                break;
            default:
                rect = new Rect(_indentionPx, _indentionPx, _subScreenLength, _subScreenHeight);
                break;
        }

        var textHeight = (int)(_pixelsInPercent * textHeightInPercents);

        GUIStyle style = new GUIStyle();

        style.alignment = tAnchor;

        style.fontSize = textHeight;

        style.normal.textColor = color;

        GUI.Label(rect, text, style);
    }
}
