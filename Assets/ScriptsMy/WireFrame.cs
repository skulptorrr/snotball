﻿using UnityEngine;
using System.Collections;

public class WireFrame : MonoBehaviour
{

    private GameSettings Gs;

    void OnPreRender() 
    {
        GL.wireframe = true;
    }
    void OnPostRender() {
        GL.wireframe = false;
    }

	// Use this for initialization
    void Start()
    {
        Gs = GameObject.Find("GameSettings").GetComponent<GameSettings>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
