﻿using UnityEngine;
using System.Collections;

public class SoundObjScript : MonoBehaviour
{
    private MusicObjScript _gameMusic;

    public bool Enabled {
        get { return _gameMusic.SoundsEnabled; }
        set { _gameMusic.SoundsEnabled = value; }
    }

    private AudioSource[] _aSourses;

    void Awake()
    {
        _gameMusic = GameObject.Find("MusicObj").GetComponent<MusicObjScript>();

        _aSourses = GetComponents<AudioSource>();

        if (Enabled)
        {
            foreach (var _aSourse in _aSourses)
            {
                _aSourse.loop = true;
                _aSourse.Play();
            }
        }

    }

    // Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update () {
        if (Enabled && _aSourses[0].isPlaying == false)
	    {
            foreach (var _aSourse in _aSourses)
            {
                _aSourse.Play();
                _aSourse.loop = true;
            }
	    }
        else if (!Enabled)
        {
            foreach (var _aSourse in _aSourses)
            {
                _aSourse.Stop();
            }
        }
	}
}
