﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MenuBase : MonoBehaviour
{
    private float _menuWInPercents;
    private float _menuHInPercents;
    private float _menuWInPixels;
    private float _menuHInPixels;

    private float _indentationInPercents;

    public float _spaceBetweenElementsInPercents;

    public float _menuItemHeightInPercents;

    private float _pixelsInPercent;

    private float _menuItemWidth;

    private TextAnchor _menuAnchor;

    private List<Action> _actions = new List<Action>();

    private float _menuX ;

    public bool Enabled;

    public MenuBase(float menuWInPercentz, float menuHInPercentz, float indentationInPercentz, float spaceBetweenElementsInPercentz, float menuItemHeightInPercentz)
    {
        _menuWInPercents = menuWInPercentz;
        _menuHInPercents = menuHInPercentz;

        _menuWInPixels = _menuWInPercents*_pixelsInPercent;
        _menuHInPixels = _menuHInPercents*_pixelsInPercent;

        _indentationInPercents = indentationInPercentz;
        _spaceBetweenElementsInPercents = spaceBetweenElementsInPercentz;
        _menuItemHeightInPercents = menuItemHeightInPercentz;

        var screenW = Screen.width;
        //var screenH = Screen.height;

        _pixelsInPercent = (int)(screenW / 100f);

        _menuItemWidth = _menuWInPercents * _pixelsInPercent - _indentationInPercents * _pixelsInPercent * 2;

        _menuX = _indentationInPercents * _pixelsInPercent;
    }

    public void AddButton(string buttonName)
    {
        var menuItemHeight = _menuItemHeightInPercents * _pixelsInPercent;
        float menuItemX = _menuX;
        float menuItemY = CalculateYofNextElement();

        _actions.Add(() =>
        {
            GUI.Button(new Rect(menuItemX, menuItemY, _menuItemWidth, menuItemHeight), buttonName);
        });
    }

    public void AddButton(string buttonName, Action clickAction)
    {
        var menuItemHeight = _menuItemHeightInPercents*_pixelsInPercent;
        float menuItemX = _menuX;
        float menuItemY = CalculateYofNextElement();

        _actions.Add(() =>
        {
            if (GUI.Button(new Rect(menuItemX, menuItemY, _menuItemWidth, menuItemHeight), buttonName))
            {
                clickAction();
            }
        });
    }

    public void AddLabel(string labelText, GUIStyle guiStyle)
    {
        var menuItemHeight = _menuItemHeightInPercents * _pixelsInPercent;
        float menuItemX = _menuX;
        float menuItemY = CalculateYofNextElement();

        _actions.Add(() =>
        {
            GUI.Label(new Rect(menuItemX, menuItemY, _menuItemWidth, menuItemHeight), labelText, guiStyle);
        });
    }

    public void AddLabel(string labelText)
    {
        var menuItemHeight = _menuItemHeightInPercents * _pixelsInPercent;
        float menuItemX = _menuX;
        float menuItemY = CalculateYofNextElement();

        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;

        _actions.Add(() =>
        {
            GUI.Label(new Rect(menuItemX, menuItemY, _menuItemWidth, menuItemHeight), labelText, style);
        });
    }

    //public void AddToogle()
    //{
    //    GUI.Toggle()
    //}


    private float CalculateYofNextElement()
    {
        float menuItemY = (_indentationInPercents +
                     _actions.Count*(_menuItemHeightInPercents + _spaceBetweenElementsInPercents))*_pixelsInPercent;

        return menuItemY;
    }

    public void DrawAllElements()
    {
        if (Enabled)
        {
            //GUI.matrix = _originalMatrix;
            //GUI.BeginGroup(new Rect(0, 0, _menuWInPixels,_menuHInPixels));
                //GUIUtility.RotateAroundPivot(10, new Vector2(-153, 150)); 
                foreach (var action in _actions)
                {
                    action();
                }

            //GUI.EndGroup();
        }
    }

}
