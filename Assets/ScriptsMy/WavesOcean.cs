﻿using UnityEngine;
using System.Collections;

namespace OceanSurfaceEffects
{
    public class WavesOcean : MonoBehaviour
    {
        /// <summary>
        /// The ocean shader just needs the suns direction.
        /// </summary>
        [SerializeField]
        GameObject m_sun;

        /// <summary>
        /// The material to render the ocean mesh with.
        /// The Script needs to bind the data it generates
        /// like the height maps to the material.
        /// </summary>
        [SerializeField]
        Material m_oceanMat;

        /// <summary>
        /// World space y position for the see level.
        /// </summary>
        [SerializeField]
        float m_seaLevel = 0.0f;

        /// <summary>
        /// Ansiotrophic filtering on wave textures.
        /// </summary>
        [SerializeField]
        int m_ansio = 2;

        /// <summary>
        /// The distance that mipmap levels on wave textures 
        /// fades to highest mipmap.  A neg number will disable this.
        /// </summary>
        [SerializeField]
        float m_lodFadeDist = 500.0f;

        /// <summary>
        /// The resolution of the grid used for the ocean.
        /// </summary>
        [SerializeField]
        int m_resolution = 16;

        /// <summary>
        /// A higher number will push more of the mesh verts 
        /// closer to center of grid were player is. 
        /// Must be >= 1.
        /// </summary>
        [SerializeField]
        float m_bias = 2.0f;

        /// <summary>
        /// Fourier grid size. Must be pow2 number.
        /// </summary>
        [SerializeField]
        int m_fourierGridSize = 64;

        /// <summary>
        /// A higher wind speed gives greater swell to the waves.
        /// </summary>
        [SerializeField]
        float m_windSpeed = 16.0f;

        /// <summary>
        /// Scales the height of the waves.
        /// </summary>
        [SerializeField]
        float m_waveAmp = 2.0f;

        /// <summary>
        /// A lower number means the waves last longer and 
        /// will build up larger waves.
        /// </summary>
        [SerializeField]
        float m_inverseWaveAge = 0.8f;

        /// <summary>
        /// The waves are made up of 4 layers of heights
        /// at different wave lengths. These grid sizes 
        /// are basically the wave length for each layer.
        /// </summary>
        [SerializeField]
        Vector4 m_gridSizes = new Vector4(5488, 392, 28, 2);

        /// <summary>
        /// The game object that contains the mesh.
        /// </summary>
        //[SerializeField]
        GameObject m_grid;

        /// <summary>
        /// A look up table for the fresnel values.
        /// </summary>
        Texture2D m_fresnelLookUp;

        /// <summary>
        /// Wave object to generate the height maps.
        /// </summary>
        WaveSpectrumCPU m_waves;

        /// <summary>
        /// Gets the wave height at a world position.
        /// </summary>
        public float SampleHeight(Vector3 worldPos) 
        { 
            return m_waves.SampleHeight(worldPos) + m_seaLevel; 
        }

        void Start()
        {
  
            m_waves = new WaveSpectrumCPU(m_fourierGridSize, m_windSpeed, m_waveAmp, m_inverseWaveAge, m_ansio, m_gridSizes);

            if (m_resolution * m_resolution >= 65000)
            {
                m_resolution = (int)Mathf.Sqrt(65000);

                Debug.Log("Grid resolution set to high. Setting resolution to the maximum allowed(" + m_resolution.ToString() + ")");
            }

            if (m_bias < 1.0f)
            {
                m_bias = 1.0f;
                Debug.Log("Bias must not be less than 1, changing to 1");
            }

            Mesh mesh = CreateRadialGrid(m_resolution, m_resolution);

            float far = Camera.main.farClipPlane;

            m_grid = new GameObject("Ocean Grid");
            m_grid.AddComponent<MeshFilter>();
            m_grid.AddComponent<MeshRenderer>();
            m_grid.GetComponent<Renderer>().material = m_oceanMat;
            m_grid.GetComponent<MeshFilter>().mesh = mesh;

            //Make radial grid have a radius equal to far plane
            m_grid.transform.localScale = new Vector3(far, 1, far); 

            m_oceanMat.SetVector("_GridSizes", m_waves.gridSizes);
            //m_oceanMat.SetFloat("_MaxLod", m_waves.mipMapLevels);
			m_oceanMat.SetTexture("_Map0", m_waves.map0);
			//m_oceanMat.SetTexture("_Map1", m_waves.map1);
			//m_oceanMat.SetTexture("_Map2", m_waves.map2);

            // VINNIE's hack for correct mesh location on the level
            m_grid.transform.rotation = new Quaternion(m_grid.transform.rotation.x, -0.4f, m_grid.transform.rotation.z, m_grid.transform.rotation.w);
        }

        void Update()
        {

			if(m_waves.done)
			{
				//write the results into the maps from the last
				//time simulate was called.
				m_waves.WriteResults();

				//Start a new simulation on a new thread.
            	m_waves.SimulateWaves(Time.realtimeSinceStartup);
			}

            //Update shader values that may change every frame
            m_oceanMat.SetVector("_SunDir", m_sun.transform.forward * -1.0f);
            m_oceanMat.SetVector("_SunColor", m_sun.GetComponent<Light>().GetComponent<Light>().color);
            m_oceanMat.SetFloat("_LodFadeDist", m_lodFadeDist);

            //This makes sure the grid is always centered were the player is
            Vector3 pos = Camera.main.transform.position;
            pos.y = m_seaLevel;

            m_grid.transform.localPosition = pos;

        }

        /// <summary>
        /// Creates a radial grid with the required segments.
        /// </summary>
        Mesh CreateRadialGrid(int segementsX, int segementsY)
        {

            Vector3[] vertices = new Vector3[segementsX * segementsY];
            Vector3[] normals = new Vector3[segementsX * segementsY];
            Vector2[] texcoords = new Vector2[segementsX * segementsY]; // not used atm

            // VINNIE's hack for build correct mesh. Original was 2.0f
            float TAU = Mathf.PI * 0.4f;
            float r;
            for (int x = 0; x < segementsX; x++)
            {
                for (int y = 0; y < segementsY; y++)
                {
                    r = (float)x / (float)(segementsX - 1);
                    // VINNIE's Fix of too big radius of the ocean
                    r = r/3;
                    r = Mathf.Pow(r, m_bias);

                    normals[x + y * segementsX] = new Vector3(0, 1, 0);

                    vertices[x + y * segementsX].x = r * Mathf.Cos(TAU * (float)y / (float)(segementsY - 1));
                    vertices[x + y * segementsX].y = 0.0f;
                    vertices[x + y * segementsX].z = r * Mathf.Sin(TAU * (float)y / (float)(segementsY - 1));
                }
            }

            int[] indices = new int[segementsX * segementsY * 6];

            int num = 0;
            for (int x = 0; x < segementsX - 1; x++)
            {
                for (int y = 0; y < segementsY - 1; y++)
                {
                    indices[num++] = x + y * segementsX;
                    indices[num++] = x + (y + 1) * segementsX;
                    indices[num++] = (x + 1) + y * segementsX;

                    indices[num++] = x + (y + 1) * segementsX;
                    indices[num++] = (x + 1) + (y + 1) * segementsX;
                    indices[num++] = (x + 1) + y * segementsX;

                }
            }

            Mesh mesh = new Mesh();

            mesh.vertices = vertices;
            mesh.uv = texcoords;
            mesh.normals = normals;
            mesh.triangles = indices;

            return mesh;

        }

    }

}
