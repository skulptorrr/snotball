﻿using System;
using UnityEngine;

public class MusicObjScript : MonoBehaviour {

    public AudioClip[] MusicArray;

    private AudioSource _audioSource;

    private int[] _lastPlayed2Songs = {999,999,999,999};

    [Header("Music Settings")]

    [SerializeField]
    private bool _musicMusicEnabled;
    public bool MusicEnabled
    {
        get { return _musicMusicEnabled; }
        set
        {
            _musicMusicEnabled = value;
            
            if (value)
            {
                PlayNextMelody();
            }
            else
            {
                _audioSource.Stop();
            }
        }
    }

    public bool SoundsEnabled;

    private void PlayBackgroundMusic()
    {
        if (MusicEnabled && !_audioSource.isPlaying)
        {
            PlayNextMelody();
        }
        else if (MusicEnabled == false && _audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
    }

    // Use this for initialization
	void Awake ()
	{
        _audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        PlayBackgroundMusic();
	}

    private void PlayNextMelody()
    {
        System.Random rnd;
        int randomAudioNumber;

        do
        {
            rnd = new System.Random();
            randomAudioNumber = rnd.Next(0, MusicArray.Length - 1);
        }
        while (MusicArrayContainsValue(randomAudioNumber));

        LandslipArrayByNewValue(randomAudioNumber);

        _audioSource.PlayOneShot(MusicArray[randomAudioNumber]);
    }

    private bool MusicArrayContainsValue(int value)
    {
        return System.Array.IndexOf(_lastPlayed2Songs, value) > -1;
    }

    private void LandslipArrayByNewValue(int value)
    {
        for (int i = _lastPlayed2Songs.Length - 1; i > 0; i--)
        {
            _lastPlayed2Songs[i] = _lastPlayed2Songs[i - 1];
        }

        _lastPlayed2Songs[0] = value;
    }

    public void ChangeMusicEnableStatus()
    {
        _musicMusicEnabled = !_musicMusicEnabled;

        PlayerPrefs.SetInt("musicEnabled", Convert.ToInt32(_musicMusicEnabled) );
        PlayerPrefs.Save();
    }

    public void ChangeSoundEnableStatus()
    {
        SoundsEnabled = !SoundsEnabled;

        PlayerPrefs.SetInt("soundsEnabled", Convert.ToInt32(SoundsEnabled));
        PlayerPrefs.Save();
    }
}
