﻿using UnityEngine;
using UnityEngine.UI;

public class MenuActions : MonoBehaviour
{

    private MusicObjScript _musicObj;

    public void Start()
    {
        _musicObj = transform.Find("MusicObj").gameObject.GetComponent<MusicObjScript>();
    }

    public void LoadFirstLevel()
    {
        LoadLevel("OceanIndie");
    }

    public void LoadLevel(string level)
    {
        MenuBehavior.CurrMenu = ActiveMenu.Loading;

        LeanTween.delayedCall(0.5f, () => { Application.LoadLevel(level); });
    }

    public void OnQualitySettingsChange()
    {
        var dd = transform.Find("MainMenuPanel/DropdownQualitySettings").GetComponent<Dropdown>();

        QualitySettings.SetQualityLevel(dd.value, true);

        Debug.Log("Quality preferences was changed");
    }

    public void ClearSettings()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        Debug.Log("game preferences was deleted");
    }

}
