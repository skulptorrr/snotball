﻿using UnityEngine;
using UnityEngine.UI;

public class ResultsScreenAnimator : MonoBehaviour {

    private Image[] _filledStars;
    private ParticleSystem _particleSystem;

    private int _scoreCounter = 0;
    public int UserScore = 8000;
    public int Speed = 10;
    public bool Enabled;

    public int[] StarsScores = {3000, 4000, 5000};

    private AudioSource _audioPoof;
    private AudioSource _audioScore;

    private Text _scoreText;
    private Text _maxScoreText;

    private void Awake()
    {
        _particleSystem = gameObject.transform.FindChild("StarsPanel/Particle System").gameObject.GetComponent<ParticleSystem>();

        _filledStars = FindGameObjects("StarsPanel/Empty{0}/Filled{0}", 3);

        _scoreText = gameObject.transform.Find("StarsPanel/TxtScore").gameObject.GetComponent<Text>();
        _maxScoreText = gameObject.transform.Find("StarsPanel/TxtMaxScore").gameObject.GetComponent<Text>();
        

        _audioScore = gameObject.GetComponents<AudioSource>()[0];
        _audioPoof = gameObject.GetComponents<AudioSource>()[01];

        HideAllFilledStars();
    }

    private void Start()
    {
        CanvasAutoconfig();
    }

    private void CanvasAutoconfig()
    {
        var mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
        var canvas = GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = mainCamera;
    }

    private void HideAllFilledStars()
    {
        foreach (var star in _filledStars)
        {
            star.enabled = false;
        }
    }

    private Image[] FindGameObjects(string template, int maxNumber)
    {
        Image[] gos = new Image[maxNumber];

        for (int i = 1; i <= maxNumber; i++)
        {
            var path = string.Format(template, i);

            gos[i - 1] = gameObject.transform.Find(path).gameObject.GetComponent<Image>();
        }

        return gos;
    }

    public void FixedUpdate()
    {
        if (Enabled && _scoreCounter < UserScore)
        {
            if (!_audioScore.isPlaying)
            {
                _audioScore.Play();
            }

            AnimateScore();

            UpdateScoreTxt();
        }
    }

    private void UpdateScoreTxt()
    {
        _scoreText.text = "SCORE: " + _scoreCounter;
    }

    private void AnimateScore()
    {
        _scoreCounter += Speed;

        if (_scoreCounter > UserScore)
        {
            _scoreCounter = UserScore;
        }

        int animStage = 1;

        if (Enabled && _scoreCounter < UserScore)
        {
            if (_scoreCounter > StarsScores[2])
            {
                StarFillAnimation(_filledStars[2]);
                //MakeParticlesAnimOn(null);
                //3 Star Filled
            }
            else if (_scoreCounter > StarsScores[1])
            {
                StarFillAnimation(_filledStars[1]);
                MakeParticlesAnimOn(_filledStars[2]);
                //2 Star Filled!
            }
            else if (_scoreCounter > StarsScores[0])
            {
                StarFillAnimation(_filledStars[0]);
                MakeParticlesAnimOn(_filledStars[1]);
                //1 Star Filled!
            }
            else
            {
                MakeParticlesAnimOn(_filledStars[0]);
                //All stars is empty
            }
        }
        else
        {
            _particleSystem.Stop();
            _audioScore.Stop();
            Enabled = false;
        }
    }

    private void MakeParticlesAnimOn(Image img)
    {
        if (img == null) return;

        if (_particleSystem.transform.position != img.transform.position)
        {
            _particleSystem.transform.position = img.transform.position;

            if (!_particleSystem.isPlaying)
            {
                _particleSystem.Play();
            }
        }
    }

    private void StarFillAnimation(Image img)
    {
        if (!img.enabled)
        {
            img.enabled = true;
            img.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            LeanTween.scale(img.gameObject, new Vector3(1f, 1f, 1f), 0.5f).setUseEstimatedTime(true).setEase(LeanTweenType.easeOutElastic);
            _audioPoof.Play();
        }
    }

    public void StopAnimation()
    {
        StarFillAnimation(_filledStars[0]);
        StarFillAnimation(_filledStars[1]);
        StarFillAnimation(_filledStars[2]);
        _particleSystem.Stop();
        _audioScore.Stop();

        _scoreCounter = UserScore;
        UpdateScoreTxt();
    }

}
