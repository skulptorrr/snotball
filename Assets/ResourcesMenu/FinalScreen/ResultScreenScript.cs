﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultScreenScript : MonoBehaviour
{
    private MenuManager _menuLib;

    public bool Enabled
    {
        get { return gameObject.activeInHierarchy; }
        set
        {
             _animationStage = 0;
             gameObject.SetActive(value);
        }
    }


    private int _animationStage = 0;

    private Text _scoreText;
    private Text _maxScoreText;

    private Image _star1;
    private Image _star2;
    private Image _star3;

    private ParticleSystem _particles1;
    private ParticleSystem _particles2;
    private ParticleSystem _particles3;

    private int _scoreCounter;
    private AudioSource _scoreCalcSound;
    private AudioSource _starPoofSound;
    
    public int UserScore = 20000;
    
    private void Awake()
    {
        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

        _star1 = GameObject.Find("Star1Filled").GetComponent<Image>();
        _star2 = GameObject.Find("Star2Filled").GetComponent<Image>();
        _star3 = GameObject.Find("Star3Filled").GetComponent<Image>();

        _particles1 = GameObject.Find("Star1Particles").GetComponent<ParticleSystem>();
        _particles2 = GameObject.Find("Star2Particles").GetComponent<ParticleSystem>();
        _particles3 = GameObject.Find("Star3Particles").GetComponent<ParticleSystem>();

        _scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        _maxScoreText = GameObject.Find("MaxScoreText").GetComponent<Text>();

        _scoreCalcSound = GetComponent<AudioSource>();

        var aSources = GetComponents<AudioSource>();
        _scoreCalcSound = aSources[0];
        _starPoofSound = aSources[1];

        _maxScoreText.text = "MAX SCORE: " + PlayerPrefs.GetInt(Application.loadedLevelName + "_MaxScore");
    }

	void FixedUpdate () 
    {
        if (Enabled)
	    {
            AnimateScore(10000, 25000, 40000, 238);

            if (Input.anyKey)
            {
                _animationStage = 8;
            }
	    }
	}

    private void AnimateScore(int star1Score, int star2Score, int star3Score, int speed)
    {
        if (_animationStage <= 10)
        {
            switch (_animationStage)
            {
                case 0:
                    _scoreCalcSound.Play();

                    _particles1.Play();

                    _animationStage += 1;
                    break;

                case 1:
                    if (_scoreCounter < star1Score && _scoreCounter < UserScore)
                    {
                        _scoreCounter += speed;
                        UpdateScoreText();
                    }
                    else _animationStage += 1;

                    break;

                case 2:
                    _particles1.Stop();
                    _particles2.Play();

                    if (_scoreCounter > star1Score)
                    {
                        _star1.enabled = true;
                        _star1.transform.localScale = new Vector3(0.1f,0.1f,0.1f);
                        LeanTween.scale(_star1.gameObject, new Vector3(1f, 1f, 1f), 0.5f).setUseEstimatedTime(true).setEase(LeanTweenType.easeOutElastic);
                        _starPoofSound.Play();
                    }
                    
                    _animationStage += 1;
                    break;

                case 3:
                    if (_scoreCounter < star2Score && _scoreCounter < UserScore)
                    {
                        _scoreCounter += speed;
                        UpdateScoreText();
                    }
                    else _animationStage += 1;

                    break;
                case 4:
                    _particles2.Stop();
                    _particles3.Play();

                    if (_scoreCounter > star2Score)
                    {
                        _star2.enabled = true;
                        _star2.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                        LeanTween.scale(_star2.gameObject, new Vector3(1f, 1f, 1f), 0.5f).setUseEstimatedTime(true).setEase(LeanTweenType.easeOutElastic);
                        _starPoofSound.Play();
                    }

                    _animationStage += 1;
                    break;
                case 5:
                    if (_scoreCounter < star3Score && _scoreCounter < UserScore)
                    {
                        _scoreCounter += speed;
                        UpdateScoreText();
                    }
                    else _animationStage += 1;

                    break;

                case 6:
                    if (_scoreCounter >= star3Score)
                    {
                        _star3.enabled = true;
                        _star3.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                        LeanTween.scale(_star3.gameObject, new Vector3(1f, 1f, 1f), 0.5f).setUseEstimatedTime(true).setEase(LeanTweenType.easeOutElastic);
                        _starPoofSound.Play();
                    }
                    
                    _animationStage += 1;
                    break;

                case 7:
                    if (_scoreCounter < UserScore)
                    {
                        _scoreCounter += speed;
                        UpdateScoreText();
                    }
                    else _animationStage += 1;

                    break;
                    
                case 8:
                    _particles1.Stop();
                    _particles2.Stop();
                    _particles3.Stop();

                    if (_scoreCounter >= star3Score) _star3.enabled = true;
                    if (_scoreCounter >= star2Score) _star2.enabled = true;
                    if (_scoreCounter >= star1Score) _star1.enabled = true;

                    _scoreCounter = UserScore;

                    UpdateScoreText();

                    _scoreCalcSound.Stop();

                    _animationStage = -1;

                    int maxScore = PlayerPrefs.GetInt(Application.loadedLevelName+"_MaxScore");
                    if (maxScore < UserScore)
                    {
                        PlayerPrefs.SetInt(Application.loadedLevelName + "_MaxScore", UserScore);
                        _maxScoreText.text = "MAX SCORE: " + UserScore;
                    }

                    break;

                default:
                    if (Input.anyKey)
                    {
                        Enabled = false;
                        _menuLib.MainMenuObj.LoadMainMenuLevel();
                    }
                    break;
            }
        }
    }

    private void UpdateScoreText()
    {
        _scoreText.text = "SCORE: " + _scoreCounter;
    }






}
