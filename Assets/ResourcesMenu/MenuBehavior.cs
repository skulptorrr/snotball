﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MenuBehavior : MonoBehaviour {

    private MenuAnimatorNeo _mmHelpAnimator;
    private MenuAnimatorNeo _mmMusicAnimator;
    private MenuAnimatorNeo _mmSoundAnimator;
    private MenuAnimatorNeo _playSingleAnimator;
    private MenuAnimatorNeo _playSingleDeviceAnimator;
    private MenuAnimatorNeo _playWiFiAnimator;

    private MenuAnimatorNeo _helpMenuAnimator;
    private MenuAnimatorNeo _inGameMenuAnimator;
    private MenuAnimatorNeo _loadingAnimator;
    private MenuAnimatorNeo _pauseMenuAnimator;
    private MenuAnimatorNeo _singlePlayerMenuAnimator;
    private MenuAnimatorNeo _wifiGameMenuAnimator;

    private MenuAnimatorNeo _grayScreen;

    private MusicObjScript _musicObj;

    /// Currently shown menu
    public static ActiveMenu CurrMenu = ActiveMenu.Main;

    /// Needed to check that CurrMenu waschanged
    private ActiveMenu _lastMenuForCheck = ActiveMenu.Pause;

    /// Needed to go back to correct menu (as example to pause or to main)
    private ActiveMenu _lastMenu = ActiveMenu.Main;

    //needed to choose in buttons
    public void SetActiveMenu(string menu)
    {
        CurrMenu = (ActiveMenu)Enum.Parse(typeof(ActiveMenu), menu, true);
    }

    public void SetLastActiveMenu()
    {
        CurrMenu = _lastMenu;
    }

    // Use this for initialization
	void Start ()
	{
        DontDestroyOnLoad(this);
        _musicObj = transform.Find("MusicObj").gameObject.GetComponent<MusicObjScript>();

	    MainMenuInit();

        _helpMenuAnimator = new MenuAnimatorNeo(transform.Find("HelpMenuPanel").gameObject, MaSide.Top, 2000);
        _helpMenuAnimator.SideHideNotAnimated();

        _inGameMenuAnimator = new MenuAnimatorNeo(transform.Find("InGamePanel").gameObject, MaSide.Top, 2000);
        _inGameMenuAnimator.SideHideNotAnimated();

        _pauseMenuAnimator = new MenuAnimatorNeo(transform.Find("PauseMenuPanel").gameObject, MaSide.Left, 1000);
        _pauseMenuAnimator.SideHideNotAnimated();

        _grayScreen = new MenuAnimatorNeo(transform.Find("GrayScreen").gameObject, MaSide.Top, 0);
	    _grayScreen.AlphaHideNotAnimated();

        _singlePlayerMenuAnimator = new MenuAnimatorNeo(transform.Find("SinglePlayerMenuPanel").gameObject, MaSide.Top, 2000);
        _singlePlayerMenuAnimator.SideHideNotAnimated();

        _wifiGameMenuAnimator = new MenuAnimatorNeo(transform.Find("WiFiGamePanel").gameObject, MaSide.Top, 2000);
        _wifiGameMenuAnimator.SideHideNotAnimated();

        _loadingAnimator = new MenuAnimatorNeo(transform.Find("LoadingPanel").gameObject, MaSide.Top, 2000);
	    _loadingAnimator.SideHideNotAnimated();

	    if (Application.loadedLevelName == "MainMenu")
        {
            Show(ActiveMenu.Main);
	    }
	    else
	    {
	        Show(ActiveMenu.InGame);
	    }
	}

    private void MainMenuInit()
    {
        _playSingleAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/PlaySingle").gameObject, MaSide.Bottom, 1000);
        _playSingleDeviceAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/PlaySingleDevice").gameObject, MaSide.Bottom, 1000);
        _playWiFiAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/PlayWiFi").gameObject, MaSide.Bottom, 1000);
        
        _mmHelpAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/mmHelp").gameObject, MaSide.Left, 600);
        _mmMusicAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/mmMusicDisable").gameObject, MaSide.Left, 800);
        _mmSoundAnimator = new MenuAnimatorNeo(transform.Find("MainMenuPanel/mmSoundDisable").gameObject, MaSide.Left, 1000);

        //Quality selector init
        var dd = transform.Find("MainMenuPanel/DropdownQualitySettings").GetComponent<Dropdown>();
        for (int i = 0; i <= QualitySettings.names.Length - 1; i++)
        {
            dd.options.Add(new Dropdown.OptionData(QualitySettings.names[i]));
        }
        dd.value = QualitySettings.GetQualityLevel();

        var mmMusicToogle =_mmMusicAnimator.gameObject.GetComponent<Toggle>();
        mmMusicToogle.isOn = !(PlayerPrefs.GetInt("musicEnabled") == 1);

        var mmSoundToogle = _mmSoundAnimator.gameObject.GetComponent<Toggle>();
        mmSoundToogle.isOn = !(PlayerPrefs.GetInt("soundsEnabled") == 1);
    }


    // Update is called once per frame
	void Update ()
	{
	    UpdateMenuIfNeeded();

	    if (Input.GetKey(KeyCode.A))
	    {
	        Show(ActiveMenu.Main);
	    }
	}


    private void UpdateMenuIfNeeded()
    {
        if (_lastMenuForCheck != CurrMenu)
        {
            Hide(_lastMenuForCheck);
            Show(CurrMenu);

            _lastMenu = _lastMenuForCheck;
            _lastMenuForCheck = CurrMenu;

            Debug.Log(string.Format("{0} menu was hidden and {1} was shown ", _lastMenu.ToString().ToUpperInvariant(), CurrMenu.ToString().ToUpperInvariant()));
        }
    }

    private float Hide(ActiveMenu menu)
    {
        switch (menu)
        {
            case (ActiveMenu.Main):
                _playSingleAnimator.SideHide();
                _playSingleDeviceAnimator.SideHide();
                _playWiFiAnimator.SideHide();

                _mmSoundAnimator.SideHide();
                _mmMusicAnimator.SideHide();
                _mmHelpAnimator.SideHide();
                return 1f;
            case (ActiveMenu.Help):
                _grayScreen.AlphaHide();
                return _helpMenuAnimator.SideHide();
            case (ActiveMenu.InGame):
                return _inGameMenuAnimator.SideHide();
            case (ActiveMenu.Loading):
                return _loadingAnimator.SideHideNotAnimated();
            case (ActiveMenu.SinglePlayer):
                _grayScreen.AlphaHide();
                return _singlePlayerMenuAnimator.SideHide();
            case (ActiveMenu.WiFiGame):
                _grayScreen.AlphaHide();
                return _wifiGameMenuAnimator.SideHide();
            case(ActiveMenu.Pause):
                return _pauseMenuAnimator.SideHide();
        }

        return 0;
    }

    private float Show(ActiveMenu menu)
    {
        switch (menu)
        {
            case (ActiveMenu.Main):
                _playSingleAnimator.SideShow(new MaConfig(){DelayAfter = 1f});
                _playWiFiAnimator.SideShow(new MaConfig() { DelayBefore = 0.1f, DelayAfter = 0.8f });
                _playSingleDeviceAnimator.SideShow(new MaConfig() { DelayBefore = 0.2f, DelayAfter = 0.9f });

                _mmSoundAnimator.SideShow(new MaConfig() { DelayBefore = 0.8f, DelayAfter = 0.2f });
                _mmMusicAnimator.SideShow(new MaConfig() { DelayBefore = 0.9f, DelayAfter = 0.1f });
                _mmHelpAnimator.SideShow(new MaConfig() { DelayBefore = 1f });

                return 2f;
            case (ActiveMenu.Help):
                _grayScreen.AlphaShow();
                return _helpMenuAnimator.SideShow();
            case (ActiveMenu.InGame):
                return _inGameMenuAnimator.SideShow();
            case (ActiveMenu.Loading):
                return _inGameMenuAnimator.ShowNotAnimated();
            case (ActiveMenu.SinglePlayer):
                _grayScreen.AlphaShow();
                return _singlePlayerMenuAnimator.SideShow();
            case (ActiveMenu.WiFiGame):
                _grayScreen.AlphaShow();
                return _wifiGameMenuAnimator.SideShow();
            case (ActiveMenu.Pause):
                return _pauseMenuAnimator.SideShow();
        }

        return 0;
    }


}

public enum ActiveMenu
{
    Main,
    Pause,
    InGame,
    Loading,
    Help,
    SinglePlayer,
    WiFiGame,
    None
}