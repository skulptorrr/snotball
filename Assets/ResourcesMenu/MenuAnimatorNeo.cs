﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAnimatorNeo
{
    public GameObject gameObject;
    public RectTransform RectTransform;
    private float _originalAlpha;

    private Vector3 _originalPosition;

    private ButtonBlocking _blockBtnsDueAnimGoing;

    private float _distance;

    private List<Button> _buttonsDetected = new List<Button>();

    private bool _buttonsIsBlocked
    {
        set
        {
            if (value)
            {
                foreach (var button in _buttonsDetected)
                {
                    button.interactable = false;
                }
            }
            else
            {
                foreach (var button in _buttonsDetected)
                {
                    button.interactable = true;
                }
            }

        }
    }

    private MaConfig _showConfig = new MaConfig()
    {
        AnimTime = 1f,

        DelayBefore = 0f,
        DelayAfter = 0f,

        Animation = LeanTweenType.easeOutElastic,
        AnimationSide = MaSide.Left
    };

    private MaConfig _hideSettings = new MaConfig()
    {
        AnimTime = 0.3f,

        DelayBefore = 0f,
        DelayAfter = 0f,

        Animation = LeanTweenType.easeInExpo,
        AnimationSide = MaSide.Left
    };

    public void ChangeAnimConfig(AnimConfig animConfig, MaConfig newOne)
    {
        switch (animConfig)
        {
            case(AnimConfig.Show):
            {
                _showConfig = MergeActions(_showConfig, newOne);
                break;
            }
            case (AnimConfig.Hide):
            {
                _hideSettings = MergeActions(_hideSettings, newOne);
                break;
            }
            case (AnimConfig.Both):
            {
                _showConfig = MergeActions(_showConfig, newOne);
                _hideSettings = MergeActions(_hideSettings, newOne);
                break;
            }
        }
    }

    public MenuAnimatorNeo(GameObject go, MaSide maSide, float distance, ButtonBlocking buttonBlockingType = ButtonBlocking.This)
    {
        gameObject = go;
        RectTransform = go.GetComponent<RectTransform>();
        var img = go.GetComponent<Image>();
        _originalAlpha = (img==null)? 0 :img.color.a;

        _originalPosition = gameObject.transform.localPosition;

         _blockBtnsDueAnimGoing = buttonBlockingType;
        _distance = distance;

        _showConfig.AnimationSide = _hideSettings.AnimationSide = maSide;

        CollectChildrenBtns(buttonBlockingType);
    }

    private void CollectChildrenBtns(ButtonBlocking buttonBlockingType)
    {
        Button btn;

        switch (buttonBlockingType)
        {
            case (ButtonBlocking.This):
            {
                AddButton(gameObject.GetComponent<Button>());
                break;
            }
            case (ButtonBlocking.Children):
            {
                for (int i = 0; i < gameObject.transform.childCount; i++)
                {
                    btn = gameObject.transform.GetChild(i).gameObject.GetComponent<Button>();
                    AddButton(btn);
                }
                break;
            }
        }
    }

    private void AddButton(Button btn)
    {
        if (btn != null)
        {
            _buttonsDetected.Add(btn);
        }
    }

    private MaConfig MergeActions(MaConfig main, MaConfig additional)
    {
        MaConfig result = new MaConfig();

        result.AnimTime = (additional.AnimTime == -1f) ? main.AnimTime : additional.AnimTime;

        result.DelayBefore = (additional.DelayBefore == -1f) ? main.DelayBefore : additional.DelayBefore;
        result.DelayAfter = (additional.DelayAfter == -1f) ?  main.DelayAfter : additional.DelayAfter;

        result.Animation = (additional.Animation != LeanTweenType.once) ? additional.Animation : main.Animation;

        result.AnimationSide = (additional.AnimationSide != MaSide.NotSelected)
            ? additional.AnimationSide
            : main.AnimationSide;

        result.BlockBtnsDueAnimGoing = (additional.BlockBtnsDueAnimGoing == ButtonBlocking.NotSelected)
            ? main.BlockBtnsDueAnimGoing
            : additional.BlockBtnsDueAnimGoing;

        return result;
    }

    public float SideShow(MaConfig config = null)
    {
        MaConfig curr = (config == null) ? _showConfig : MergeActions(_showConfig, config);

        SideHideNotAnimated(curr.AnimationSide);

        return MoveToPosition(curr, _originalPosition);
    }

    public float SideHideNotAnimated()
    {
        SideHideNotAnimated(_hideSettings.AnimationSide);
        return 0;
    }

    private void SideHideNotAnimated(MaSide side)
    {
        gameObject.transform.localPosition = CalcNewVector(side);
    }

    public float SideHide(MaConfig config = null)
    {
        MaConfig curr = (config == null) ? _showConfig : MergeActions(_showConfig, config);

        ShowNotAnimated();

        return MoveToPosition(curr, CalcNewVector(curr.AnimationSide));
    }

    public float ShowNotAnimated()
    {
        gameObject.transform.localPosition = _originalPosition;
        return 0;
    }

    private Vector3 CalcNewVector(MaSide side)
    {
        switch (side)
        {
            case (MaSide.Left):
                {
                    return new Vector3(_originalPosition.x - _distance, _originalPosition.y, _originalPosition.z);
                }
            case (MaSide.Right):
                {
                    return new Vector3(_originalPosition.x + _distance, _originalPosition.y, _originalPosition.z);
                }
            case (MaSide.Top):
                {
                    return new Vector3(_originalPosition.x, _originalPosition.y + _distance, _originalPosition.z);
                }
            case (MaSide.Bottom):
                {
                    return new Vector3(_originalPosition.x, _originalPosition.y - _distance, _originalPosition.z);
                }
        }

        throw new Exception("Incorrect side!");
    }

    private float MoveToPosition(MaConfig settingsSettings, Vector3 newPosition)
    {
        _buttonsIsBlocked = true;

        LeanTween.moveLocal(gameObject, newPosition, settingsSettings.AnimTime).setEase(settingsSettings.Animation).setIgnoreTimeScale(true).setDelay(settingsSettings.DelayBefore);

        float fullTime = settingsSettings.AnimTime + settingsSettings.DelayBefore + settingsSettings.DelayAfter;
        LeanTween.delayedCall(fullTime, () => { _buttonsIsBlocked = false; });
        return fullTime;
    }

    public float AlphaHide(float time = 0.3f)
    {
        LeanTween.alpha(RectTransform, 0, time).setIgnoreTimeScale(true);
        return time;
    }

    public float AlphaShow(float time = 1f)
    {
        LeanTween.alpha(RectTransform, _originalAlpha, time).setIgnoreTimeScale(true);
        return time;
    }

    public float AlphaHideNotAnimated()
    {
        return AlphaHide(0.01f);
    }


    public float AlphaShowNotAnimated()
    {
        return AlphaShow(0.01f);
    }


}


public class MaConfig
{
    public float AnimTime = -1f;
    public float DelayBefore = -1f;
    public float DelayAfter = -1f;
    public LeanTweenType Animation = LeanTweenType.once;
    public ButtonBlocking BlockBtnsDueAnimGoing = ButtonBlocking.NotSelected;
    public MaSide AnimationSide = MaSide.NotSelected;
}

public enum ButtonBlocking
{
    This,
    Children,
    None,
    NotSelected
}

public enum MaSide
{
    Left,
    Right,
    Top,
    Bottom,
    NotSelected
}

public enum AnimConfig
{
    Show,
    Hide,
    Both
}