﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{
    [Header("Menu list")]
    public MainMenu MainMenuObj;
    public InGameScript InGameMenuObj;
    public PauseScript PauseMenuObj;
    public SinglePlayerMenu SinglePlayerMenuObj;
    public GameObject HelpScreenObj;
    public GameObject LoadingScreenObj;
    public ResultScreenScript ResultScreenMenu;
    public WiFiGameScript WiFiGameMenu;
}
