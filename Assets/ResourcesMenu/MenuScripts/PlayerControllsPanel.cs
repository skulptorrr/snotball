﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerControllsPanel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public KeyCode ActionKeyCode;

    [HideInInspector]
    public bool UserAsksForAction;

    [HideInInspector]
    public bool IsHuman = true;

    //Working with mouse clicks and touch
    public void OnPointerDown(PointerEventData eventData)
    {
        UserAsksForAction = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        UserAsksForAction = false;
    }
    
    // Working with keyboard
	void Update () {
	    if (IsHuman)
	    {
            if (Input.GetKeyDown(ActionKeyCode))
            {
                UserAsksForAction = true;
            }
            if (Input.GetKeyUp(ActionKeyCode))
            {
                UserAsksForAction = false;
            }
	    }
	}
}
