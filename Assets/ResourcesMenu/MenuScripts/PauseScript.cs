﻿using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour
{
    private MenuManager _menuLib;
    
    private GameplayBallBehavior _gameplayBallBehavior;

    private MusicObjScript _gameMusicObj;
    private Image _gameMusicDisabledLine;
    private Image _gameSoundsDisabledLine;

    private MenuAnimator _pausemMenuAnimator;

    public float HideAnimationLength = 1f;
    public float ShowAnimationLength = 0.2f;

    public bool Enabled
    {
        get { return _menuLib.PauseMenuObj.isActiveAndEnabled; }
        set {gameObject.SetActive(value);}
    }

    public bool EnabledWithAnimation
    {
        set
        {
            if (value)
            {
                Enabled = value;
                if (_pausemMenuAnimator != null)
                {
                    _pausemMenuAnimator.LeftHide();
                _pausemMenuAnimator.ShowAnimation(HideAnimationLength);
                }
            }
            else
            {
                _pausemMenuAnimator.LeftHideAnimation(ShowAnimationLength);
                LeanTween.delayedCall(ShowAnimationLength, Resume).setUseEstimatedTime(true);
            }
        }
    }
    private void Resume()
    {
        _menuLib.InGameMenuObj.EnabledWithAnimation = true;
        Time.timeScale = 1;
        Enabled = false;
    }

    private GameObject _pauseMenuPanel;
    void Awake()
    {
        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();
        
        _gameMusicObj = GameObject.Find("MusicObj").GetComponent<MusicObjScript>();
        _gameMusicDisabledLine = GameObject.Find("PauseMusicDisabled").GetComponent<Image>();
        _gameSoundsDisabledLine = GameObject.Find("PauseSoundDisabled").GetComponent<Image>();

        _pauseMenuPanel = GameObject.Find("PauseBackground");


    }

    void Start()
    {
        _pausemMenuAnimator = new MenuAnimator(_pauseMenuPanel, 1000);
    }

    public void RestartGameSession()
    {
        GameObject.Find("Ball").GetComponent<GameplayBallBehavior>().StartNewMatch();
        EnabledWithAnimation = false;
    }

    public void EnableDisableMusic()
    {
        _gameMusicObj.ChangeMusicEnableStatus();
    }

    public void EnableDisableSound()
    {
        _gameMusicObj.ChangeSoundEnableStatus();
    }

    public void GoToMainMenu()
    {
        _pausemMenuAnimator.LeftHideAnimation(ShowAnimationLength);

        _menuLib.LoadingScreenObj.SetActive(true);

        LeanTween.delayedCall(_menuLib.MainMenuObj.gameObject, 1f, _menuLib.MainMenuObj.LoadMainMenuLevel);
    }
    

    // Update is called once per frame
	void Update () {
        if (Enabled)
        {
            _gameMusicDisabledLine.enabled = !_gameMusicObj.MusicEnabled;
            _gameSoundsDisabledLine.enabled = !_gameMusicObj.SoundsEnabled;
        }
	}

    private void PlayCloseAnimation()
    {
    }

    private void PlayAnimation()
    {   
    }
}
