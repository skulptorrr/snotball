﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SinglePlayerMenu : MonoBehaviour
{
    private MenuManager _menuLib;

    private MenuAnimator _spmAnimator ;

    private Button _quitButton;

    void Awake()
    {
        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

        _quitButton = GameObject.Find("BackToMm").GetComponent<Button>();

        _spmAnimator = new MenuAnimator(_menuLib.SinglePlayerMenuObj.gameObject, 1500);
    }

    public bool Enabled
    {
        get { return gameObject.activeInHierarchy; }
        set { gameObject.SetActive(value); }
    }

    public bool EnabledAnimated
    {
        set
        {
            if (value)
            {
                Enabled = value;
                _spmAnimator.TopHide();
                _spmAnimator.ShowAnimation();
                LeanTween.delayedCall(1f, EnableBackButton);
            }
            else
            {
                _spmAnimator.TopHideAnimation(1f);
                DisableBackButton();
            }
        }
    }

    public void LoadFirstLevel()
    {
        EnabledAnimated = false;

        LoadLevel("OceanIndie");
    }

    public void GoToMainMenu()
    {
        if (_quitButton.interactable)
        {
            _menuLib.SinglePlayerMenuObj.EnabledAnimated = false;
            _menuLib.MainMenuObj.EnabledAnimated = true;
        }
    }

    public void LoadLevel(string levelName)
    {
        Enabled = false;
        _menuLib.LoadingScreenObj.SetActive(true);

        Application.LoadLevel(levelName);
    }

    public void EnableBackButton()
    {
        if (_quitButton != null)
        {
            _quitButton.interactable = true;
        }
    }

    public void DisableBackButton()
    {
        _quitButton.interactable = false;
    }
}
