﻿using UnityEngine;
using System.Collections;


class MenuAnimator
{
    private GameObject _gameObj;

    private Vector3 __originalPosition;

    private Vector3 _originalPosition;

    private int _difference;

    private float _showDefaultTime = 1f;
    private float _hideDefaultTime = 0.3f;

    private LeanTweenType _showAnimation;
    private LeanTweenType _hideAnimation;

    public MenuAnimator(GameObject gameObject, int difference)
    {
        _gameObj = gameObject;
        _originalPosition = gameObject.transform.localPosition;

        _difference = difference;

        _showAnimation = LeanTweenType.easeOutElastic;
        _hideAnimation = LeanTweenType.easeInExpo;
    }

    public MenuAnimator(GameObject gameObject, int difference, LeanTweenType showAnimation, LeanTweenType hideAnimation)
    {
        _gameObj = gameObject;
        _originalPosition = gameObject.transform.localPosition;

        _difference = difference;

        _showAnimation = showAnimation;
        _hideAnimation = hideAnimation;
    }
    
    public void ShowAnimation()
    {
        ShowAnimation(_showDefaultTime);
    }

    public void ShowAnimation(float animLength)
    {
        LeanTween.moveLocal(_gameObj, _originalPosition, animLength).setUseEstimatedTime(true).setEase(_showAnimation);
    }

    public void LeftHide()
    {
        _gameObj.transform.localPosition = new Vector3(_originalPosition.x - _difference, _gameObj.transform.localPosition.y, _gameObj.transform.localPosition.z);
    }

    public void LeftHideAnimation()
    {
        LeftHideAnimation(_hideDefaultTime);
    }

    public void LeftHideAnimation(float animLength)
    {
        _gameObj.transform.localPosition = _originalPosition;

        LeanTween.moveLocalX(_gameObj, _originalPosition.x - _difference, animLength).setUseEstimatedTime(true).setEase(_hideAnimation);
    }

    public void TopHide()
    {
        _gameObj.transform.localPosition = new Vector3(_originalPosition.x, _originalPosition.y + _difference, _gameObj.transform.localPosition.z);
    }

    public void TopHideAnimation()
    {
        LeftHideAnimation(_hideDefaultTime);
    }

    public void TopHideAnimation(float animLength)
    {
        _gameObj.transform.localPosition = _originalPosition;

        LeanTween.moveLocalY(_gameObj, _originalPosition.y + _difference, animLength).setUseEstimatedTime(true).setEase(_hideAnimation);
    }

    public void BottomHide()
    {
        _gameObj.transform.localPosition = new Vector3(_originalPosition.x, _originalPosition.y - _difference, _gameObj.transform.localPosition.z);
    }

    public void BottomHideAnimation()
    {
        BottomHideAnimation(_hideDefaultTime);
    }

    public void BottomHideAnimation(float animLength)
    {
        _gameObj.transform.localPosition = _originalPosition;

        LeanTween.moveLocalY(_gameObj, _originalPosition.y - _difference, animLength).setUseEstimatedTime(true).setEase(_hideAnimation);
    }
}
