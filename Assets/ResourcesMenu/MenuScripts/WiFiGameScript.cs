﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WiFiGameScript : MonoBehaviour {
    private MenuManager _menuLib;
    private MenuAnimator _wifiMenuAnimator;

    private Button _quitButton;

    public bool Enabled
    {
        get { return gameObject.activeInHierarchy; }
        set { gameObject.SetActive(value); }
    }
    public bool EnabledAnimated
    {
        set
        {
            if (value)
            {
                Enabled = value;
                _wifiMenuAnimator.TopHide();
                _wifiMenuAnimator.ShowAnimation();
                LeanTween.delayedCall(1f, EnableBackButton);
            }
            else
            {
                _wifiMenuAnimator.TopHideAnimation(1f);
                DisableBackButton();
            }
        }
    }

    void Awake()
    {
        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

        _quitButton = GameObject.Find("WiFiBackToMm").GetComponent<Button>();

        _wifiMenuAnimator = new MenuAnimator(_menuLib.WiFiGameMenu.gameObject, 1500);
    }

    public void GoToMainMenu()
    {
        if (_quitButton.interactable)
        {
            EnabledAnimated = false;
            _menuLib.MainMenuObj.EnabledAnimated = true;
        }
    }
    
    public void EnableBackButton()
    {
        if (_quitButton != null)
        {
            _quitButton.interactable = true;
        }
    }

    public void DisableBackButton()
    {
        _quitButton.interactable = false;
    }

    public void HostGame()
    {
        
    }

}
