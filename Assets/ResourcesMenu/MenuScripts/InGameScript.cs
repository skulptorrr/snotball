﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameScript : MonoBehaviour {

    private MenuManager _menuLib;

    private Text _uiScoreMaxText;
    private Text _uiScore1Text;
    private Text _uiScore2text;

    public int ScoreToWin = 13;

    [HideInInspector]
    public GameObject InGameMessage;

    private MenuAnimator _inGameMenuAnimator;

    private int _player1Score;
    public int Player1Score
    {
        get { return _player1Score; }
        set { _player1Score = value; }
    }

    private int _player2Score;
    public int Player2Score
    {
        get { return _player2Score; }
        set { _player2Score = value; }
    }

    public bool Enabled
    {
        get { return gameObject.activeInHierarchy; }
        set { gameObject.SetActive(value); }
    }

    public bool EnabledWithAnimation
    {
        set
        {
            if (value)
            {
                Enabled = value;
                _inGameMenuAnimator.ShowAnimation(0.8f);
            }
            else if (!value)
            {
                _inGameMenuAnimator.TopHideAnimation(0.5f);
            }
        }
    }

    private void Disable()
    {
        Enabled = false;
    }


    void Awake()
    {
        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();
        
        _uiScoreMaxText = GameObject.Find("ScoreMax").GetComponent<Text>();
        _uiScore1Text = GameObject.Find("ScorePlayer1").GetComponent<Text>();
        _uiScore2text = GameObject.Find("ScorePlayer2").GetComponent<Text>();

        InGameMessage = GameObject.Find("InGameMessage");

        _inGameMenuAnimator = new MenuAnimator(_menuLib.InGameMenuObj.gameObject, 1800);
    }
    

    public void Pause()
    {
        Time.timeScale = 0;
        EnabledWithAnimation = false;
        _menuLib.PauseMenuObj.EnabledWithAnimation = true;
    }
    
	// Update is called once per frame
	void Update () {
        UpdateScores();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_menuLib.MainMenuObj.HelpShown)
            {
                _menuLib.MainMenuObj.HelpShown = false;
            }
            else
            {
                //if (MusicEnabled)
                //{
                //    EnabledWithAnimation = true;
                //}
                //else if (_menuLib.MainMenuObj.MusicEnabled == false)
                //{
                //    _menuLib.PauseMenuObj.Resume();
                //}   
            }
        }
	}

    private void UpdateScores()
    {
        _uiScoreMaxText.text = ScoreToWin.ToString("00");
        _uiScore1Text.text = _player1Score.ToString("00");
        _uiScore2text.text = _player2Score.ToString("00");
    }
}
