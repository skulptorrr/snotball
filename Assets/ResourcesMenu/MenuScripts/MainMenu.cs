﻿using System;
using System.Diagnostics;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    private GameObject _helpScreenPanel;

    private Button _playSingleBtnComponent;
    private Button _playWiFiMultiplayerBtnComponent;
    private Button _playSingleDeviceMultiplayerBtnComponent;
    
    private MusicObjScript _gameMusicObj;
    private Image _gameMusicDisabledLine;
    private Image _gameSoundsDisabledLine;

    private MenuManager _menuLib;

    private MainMenuAnimator _mmAnimator;

    private string _menuToOpenOnStart = "MainMenu";

    public bool Enabled
    {
        get { return gameObject.activeInHierarchy; }
        set { gameObject.SetActive(value);}
    }

    public bool EnabledAnimated
    {
        set
        {
            if (value)
            {
                Enabled = value;
                _mmAnimator.Show();
            }
            else
            {
                _mmAnimator.Hide();
                LeanTween.delayedCall(this.gameObject, 0.8f, Disable);
            }
        }
    }

    private void Disable()
    {
        Enabled = false;
    }
    
    private bool _helpShown ;
    public bool HelpShown
    {
        get { return _helpShown; }
        set
        {
            _menuLib.HelpScreenObj.SetActive(value);
            _helpShown = value;
        }
    }
    
    void Awake()
    {
        DontDestroyOnLoad(this);

        _menuLib = GameObject.Find("Menus").GetComponent<MenuManager>();

        AllMenusEnabled(true);

        _gameMusicObj = GameObject.Find("MusicObj").GetComponent<MusicObjScript>();
        _gameMusicDisabledLine = GameObject.Find("mmMusicDisabled").GetComponent<Image>();
        _gameSoundsDisabledLine = GameObject.Find("mmSoundDisabled").GetComponent<Image>();

        _playSingleBtnComponent = GameObject.Find("PlaySingle").GetComponent<Button>();
        _playWiFiMultiplayerBtnComponent = GameObject.Find("PlaySingleDeviceMultiplayer").GetComponent<Button>();
        _playSingleDeviceMultiplayerBtnComponent = GameObject.Find("PlayWiFiMultiplayer").GetComponent<Button>();

        var dd = GameObject.Find("DropdownQualitySettings").GetComponent<Dropdown>();

        for (int i = 0; i <= QualitySettings.names.Length - 1; i++)
        {
            dd.options.Add(new Dropdown.OptionData(QualitySettings.names[i]));
        }

        dd.value = QualitySettings.GetQualityLevel();

        Time.timeScale = 1;
    }
    
    // Use this for initialization
	void Start ()
	{
        _mmAnimator = new MainMenuAnimator();
        ShowCorrectMenuForCurrentScene();
	}

    private void ShowCorrectMenuForCurrentScene()
    {

		if (Application.loadedLevelName == "MainMenu")
        {
            AllMenusEnabled(false);

            switch(_menuToOpenOnStart)
            {
                case "MainMenu":
                    EnabledAnimated = true;
                    break;
                case "SinglePlayer":
                    OpenSinglePlayerGameMenu();
                    break;
            }

            
        }
		else
		{
            AllMenusEnabled(false);
            _menuLib.InGameMenuObj.EnabledWithAnimation = true;
		}
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (Enabled)
	    {
            _gameMusicDisabledLine.enabled = !_gameMusicObj.MusicEnabled;
            _gameSoundsDisabledLine.enabled = !_gameMusicObj.SoundsEnabled;
	    }

	    if (Input.GetKeyDown(KeyCode.Space))
	    {
            _mmAnimator.Hide();
	    }
	}

    public void EnableDisableMusic()
    {
        _gameMusicObj.ChangeMusicEnableStatus();
    }

    public void EnableDisableSound()
    {
        _gameMusicObj.ChangeSoundEnableStatus();
    }

    public void OpenSinglePlayerGameMenu()
    {
        if (_playSingleBtnComponent.interactable)
        {
            EnabledAnimated = false;
            _menuLib.SinglePlayerMenuObj.EnabledAnimated = true;
        }
    }

    public void OpenWiFiPanelGameMenu()
    {
        if (_playWiFiMultiplayerBtnComponent.interactable)
        {
            EnabledAnimated = false;
            _menuLib.WiFiGameMenu.EnabledAnimated = true;
        }
    }

    public void AllMenusEnabled(bool value)
    {
        _menuLib.MainMenuObj.Enabled = value;
        _menuLib.SinglePlayerMenuObj.Enabled = value;
        _menuLib.InGameMenuObj.Enabled = value;
        _menuLib.PauseMenuObj.Enabled = value;
        _menuLib.LoadingScreenObj.SetActive(value);
        _menuLib.ResultScreenMenu.Enabled = value;
        _menuLib.WiFiGameMenu.Enabled = value;
        HelpShown = value;
    }

    public void LoadMainMenuLevel()
    {
        _menuLib.SinglePlayerMenuObj.LoadLevel("MainMenu");
    }
    
    public void OnQualitySettingsChange(int i)
    {
        QualitySettings.SetQualityLevel(i, true);
    }

    public void ClearSettings()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

}

public class MainMenuAnimator
{
    private GameObject _playSingleBtn;
    private GameObject _playWiFiMultiplayerBtn;
    private GameObject _playSingleDeviceMultiplayerBtn;

    private GameObject _musicBtn;
    private GameObject _soundBtn;
    private GameObject _helpBtn;

    private Button _playSingleBtnComponent;
    private Button _playWiFiMultiplayerBtnComponent;
    private Button _playSingleDeviceMultiplayerBtnComponent;

    private MenuAnimator _playSingle;
    private MenuAnimator _playWiFiMultiplayer;
    private MenuAnimator _playSingleDeviceMultiplayer;

    private MenuAnimator _music;
    private MenuAnimator _sound;
    private MenuAnimator _help;
    
    public MainMenuAnimator()
    {
        _playSingleBtn = GameObject.Find("PlaySingle");
        _playSingleDeviceMultiplayerBtn = GameObject.Find("PlaySingleDeviceMultiplayer");
        _playWiFiMultiplayerBtn = GameObject.Find("PlayWiFiMultiplayer");

        _playSingleBtnComponent = _playSingleBtn.GetComponent<Button>();
        _playSingleDeviceMultiplayerBtnComponent = _playSingleDeviceMultiplayerBtn.GetComponent<Button>();
        _playWiFiMultiplayerBtnComponent = _playWiFiMultiplayerBtn.GetComponent<Button>();

        _musicBtn = GameObject.Find("mmMusicDisable");
        _soundBtn = GameObject.Find("mmSoundDisable");
        _helpBtn = GameObject.Find("mmHelp");

        var shift1 = 600;
        var shift2 = 950;

        _playSingle = new MenuAnimator(_playSingleBtn, shift1);
        _playSingleDeviceMultiplayer = new MenuAnimator(_playSingleDeviceMultiplayerBtn, shift1);
        _playWiFiMultiplayer = new MenuAnimator(_playWiFiMultiplayerBtn, shift1);

        _music = new MenuAnimator(_musicBtn, shift2, LeanTweenType.easeOutQuart, LeanTweenType.clamp);
        _sound = new MenuAnimator(_soundBtn, shift2, LeanTweenType.easeOutQuart, LeanTweenType.clamp);
        _help = new MenuAnimator(_helpBtn, shift2, LeanTweenType.easeOutQuart, LeanTweenType.clamp);
    }
    
    public void Show()
    {
        SetBtnsInteractableToFalse();

        _playSingle.BottomHide();
        _playWiFiMultiplayer.BottomHide();
        _playSingleDeviceMultiplayer.BottomHide();

        _help.LeftHide();
        _music.LeftHide();
        _sound.LeftHide();

        float shift = 0f;

        LeanTween.delayedCall(shift + 0.2f, _playSingle.ShowAnimation);
        LeanTween.delayedCall(shift + 0.4f, _playWiFiMultiplayer.ShowAnimation);
        LeanTween.delayedCall(shift + 0.6f, _playSingleDeviceMultiplayer.ShowAnimation);

        LeanTween.delayedCall(shift + 0.7f, _sound.ShowAnimation);
        LeanTween.delayedCall(shift + 0.8f, _music.ShowAnimation);
        LeanTween.delayedCall(shift + 0.9f, _help.ShowAnimation);
        LeanTween.delayedCall(shift + 1.5f, SetBtnsInteractableToTrue);
    }

    public void Hide()
    {
        SetBtnsInteractableToFalse();

        _playSingle.BottomHideAnimation();
        LeanTween.delayedCall(0.1f, _playWiFiMultiplayer.BottomHideAnimation);
        LeanTween.delayedCall(0.2f, _playSingleDeviceMultiplayer.BottomHideAnimation);
        
        LeanTween.delayedCall(0.1f, _help.LeftHideAnimation);
        LeanTween.delayedCall(0.2f, _music.LeftHideAnimation);
        LeanTween.delayedCall(0.3f, _sound.LeftHideAnimation);
    }

    public void SetBtnsInteractableToTrue()
    {
        _playSingleBtnComponent.interactable = true;
        _playWiFiMultiplayerBtnComponent.interactable = true;
        _playSingleDeviceMultiplayerBtnComponent.interactable = true;
    }
    public void SetBtnsInteractableToFalse()
    {
        _playSingleBtnComponent.interactable = false;
        _playWiFiMultiplayerBtnComponent.interactable = false;
        _playSingleDeviceMultiplayerBtnComponent.interactable = false;
    }
}