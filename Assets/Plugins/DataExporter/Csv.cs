﻿namespace BotAgent.Ifrit.DataExporter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Linq;

    public class Csv
    {
        private List<string> _csvFile = new List<string>();

        public char SeparatorChar;

        public int RowsCount
        {
            get { return _csvFile.Count; }
        }

        /// <summary>
        /// Init Csv file editor with default char separator
        /// </summary>
        public Csv()
        {
            SeparatorChar = ',';
        }

        /// <summary>
        /// Init Csv file editor
        /// </summary>
        /// <param name="separatorChar">Char separator on FileRead and FileSave</param>
        public Csv(char separatorChar)
        {
            SeparatorChar = separatorChar;
        }

        public void FileOpen(string path)
        {
            try
            {
                StreamReader csvRdr = new StreamReader(path);

                string text = csvRdr.ReadToEnd();

                _csvFile = new List<string>(
                    text.Split(new string[] {"\r\n"},
                        StringSplitOptions.None));

                for (int i = 0; i < _csvFile.Count; i++)
                {
                    RecursivelyFixOfRow(i);
                }

                csvRdr.Close();
            }
            catch (Exception)
            {
                //// File not exist or this is non CSV
            }

            if (_csvFile.Last() == string.Empty)
            {
                var latsItemIndex = _csvFile.Count -1;
                _csvFile.RemoveAt(latsItemIndex);
            }

        }

        public void FileSave(string path)
        {
            var correctPath = Directory.GetParent(path).ToString();

            Directory.CreateDirectory(correctPath);

            FileSave(path, false);
        }

        private void FileSave(string path, bool appendFile)
        {
            using (StreamWriter writer = new StreamWriter(path, appendFile, Encoding.UTF8))
            {
                foreach (string row in _csvFile)
                {
                    writer.WriteLine(row);
                }
            }
        }

        /// <summary>
        /// Add rows to the end of existing file
        /// </summary>
        public void FileAppend(string path)
        {
            FileSave(path, true);
        }

        public void RowAdd(params string[] rowCells)
        {
            string row = RowBuild(rowCells);

            _csvFile.Add(row);
        }

        public void RowInsert(int indexToLocateRow,params string[] rowCells)
        {
            string row = RowBuild(rowCells);

            _csvFile.Insert(indexToLocateRow, row);
        }

        public void RowsAdd(List<string[]> rowsWithValues)
        {
            foreach (var rowCells in rowsWithValues)
            {
                string row = RowBuild(rowCells);
                _csvFile.Add(row);
            }
        }

        public void RowsInsert(int indexToLocateRow, List<string[]> rowsWithValues)
        {
            List<string> rowsToAdd = new List<string>();

            foreach (var rowCells in rowsWithValues)
            {
                string row = RowBuild(rowCells);

                rowsToAdd.Add(row);
            }

            _csvFile.InsertRange(indexToLocateRow, rowsToAdd);
        }

        public bool RowRemove(params string[] rowCells)
        {
            string row = RowBuild(rowCells);

            return _csvFile.Remove(row);
        }

        public void RowRemove(int index)
        {
            _csvFile.RemoveAt(index);
        }

        public string[] RowGetValues(int index)
        {
            string row = _csvFile[index];
            
            return RowRead(row);
        }

        public void ClearRows()
        {
            _csvFile.Clear();
        }

        private string[] RowRead(string row)
        {
            if (row == string.Empty)
            { 
                return new string[0];
            }

            var charToSearch = '"';
            var foundIndexes = new List<int>();
            var indexesToRemove = new List<int>();
            List<string> rezult = new List<string>();

            //// Search for quotes char indexes
            for (int i = row.IndexOf(charToSearch); i > -1; i = row.IndexOf(charToSearch, i + 1))
            {
                foundIndexes.Add(i);
            }

            //// Check for /"/"
            var indexCounter = foundIndexes.Count - 1;
            for (int i = 0; i < indexCounter; i++)
            {
                if (foundIndexes[i+1] - foundIndexes[i] == 1)
                {
                    indexesToRemove.Add(i);
                    indexesToRemove.Add(i + 1);
                }
            }

            ////  and miss them
            for (int i = indexesToRemove.Count-1; i >= 0; i--)
            {
                foundIndexes.RemoveAt(indexesToRemove[i]);
            }

            //// Add first and last symbols indexes if needed
            if (foundIndexes.Count > 0)
            {
                if (foundIndexes[0] != 0)
                {
                    foundIndexes.Insert(0, 0);
                }
                if (foundIndexes[foundIndexes.Count - 1] != row.Length - 1)
                {
                    foundIndexes.Add(row.Length);
                }
            }
            else
            {
                foundIndexes.Insert(0, 0);
                foundIndexes.Add(row.Length);
            }

            //// Divide to subLines
            var subLinesToAnalize = foundIndexes.Count -1;
            for (int i = 0; i < subLinesToAnalize; i++)
            {
                var offset = 0;

                if (row[0] == '"')
                {
                    if (i % 2 == 0)
                        offset = 1;
                    else
                        offset = 0;
                }
                else 
                {
                    if (i % 2 == 0)
                        offset = 0;
                    else
                        offset = 1;
                }

                var subStrStart = foundIndexes[i];
                var subStrLength = foundIndexes[i + 1] + offset - subStrStart;
                
                var subStr = row.Substring(subStrStart, subStrLength);

                //// If subline contains \"\" 
                if (subStr.Contains("\""))
                {
                    subStr = subStr.Replace("\"\"", "\"").TrimStart('\"').TrimEnd('\"').TrimStart(SeparatorChar).TrimEnd(SeparatorChar);

                    rezult.Add(subStr);
                }
                else 
                {
                    subStr = subStr.TrimStart(SeparatorChar).TrimEnd(SeparatorChar);

                    string[] tmp = subStr.Split(SeparatorChar);

                    foreach (string cell in tmp)
                    {
                        rezult.Add(cell);
                    }
                }
            }

            return rezult.ToArray();
        }

        private string RowBuild(params string[] rowCells)
        {
            StringBuilder builder = new StringBuilder();

            bool firstColumn = true;

            foreach (string value in rowCells)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(SeparatorChar);

                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', SeparatorChar }) != -1)
                {
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                }
                else
                {
                    builder.Append(value);
                }

                firstColumn = false;
            }
            return builder.ToString();
        }

        private void RecursivelyFixOfRow(int index)
        {
            const char charToSearch = '"';

            var quotesCounter = _csvFile[index].Split(charToSearch).Length - 1;

            if (quotesCounter % 2 != 0)
            {
                _csvFile[index] += "\r\n" + _csvFile[index + 1];

                _csvFile.RemoveAt(index + 1);

                RecursivelyFixOfRow(index);
            }
        }

    }
}
