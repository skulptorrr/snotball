﻿using UnityEngine;
using System.Collections;

public class WavingTreesAnimation : MonoBehaviour
{
    private Renderer _renderer;
    private GameSettings _gs;

    private Vector4 _waving;
    private float _wavingPower;

	// Use this for initialization
	void Start ()
	{
	    _renderer = GetComponent<Renderer>();

	    _gs = GameObject.Find("GameSettings").GetComponent<GameSettings>();

        _waving = _renderer.material.GetVector("_WaveAndDistance");

        _wavingPower = _waving.w;
	}
	
	// Update is called once per frame
	void Update () {
        _waving.x += (Time.deltaTime * _gs.TreeAnimationWindPower * _wavingPower * Random.value);
        _waving.y -= (Time.deltaTime * _gs.TreeAnimationWindPower * _wavingPower * Random.value);

        _renderer.material.SetVector("_WaveAndDistance", new Vector4(_waving.x, _waving.y, _waving.z, _waving.w));
	}
}